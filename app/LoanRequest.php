<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanRequest extends Model
{    
    public function loan_type(){
        return $this->belongsTo('App\LoanType');
    }
    public function guarantors(){
        return $this->hasMany('App\Guarantor');
    }
    public function loan(){
        return $this->hasOne('App\Loan');
    }
    public function member_profile(){
        return $this->belongsTo('App\MemberProfile');
    }
    public function cc_feedback(){
        return $this->hasMany('App\LoanRequestCCFeedback');
    }

    public const statuses = [0=>'Deleted',1=>'Pending',2=>'Approved',3=>'Rejected',10=>'Pending Guarantor Add',11=>'Pending Guarantor Approvals',12=>'Pending Credit-committee'];
    
    public function getGuarantorRequiredAttribute(){
        return $this->loan_type_id==1;
    }
    public function getStatusTextAttribute(){
        return $this::statuses[$this->status];
    }
    public function getEditableAttribute(){
        return $this->status>0 && $this->status!=2;
    }
    public function getDeletableAttribute(){
        return $this->status>0;
    }
}
