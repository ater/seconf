<?php

namespace App\Http\Controllers;

use App\MemberProfile;
use Illuminate\Http\Request;

class MemberProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = auth()->user()->profile;
        return view('profiles.index',compact('profile'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MemberProfile  $memberProfile
     * @return \Illuminate\Http\Response
     */
    public function show(MemberProfile $memberProfile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MemberProfile  $memberProfile
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $profile = auth()->user()->profile;
        //return $profile;
        return view('profiles.edit',compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MemberProfile  $memberProfile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request,[
            'member_num' => 'required',
            'name'=> 'required',
            'national'=> 'required',
            'phone'=> 'required',
            'email'=> 'required | email',
            'postal'=> 'required'
        ]);
        
        if(auth()->user()->email == $request->input('email')){
            try {            
                //Get Member
                $member = MemberProfile::where('id',$request->input('member_num') )->where('user_id',auth()->user()->id)->first();
                if($member){                    
                    $member->fullname = $request->input('name');
                    $member->national_id = $request->input('national');
                    $member->phone = $request->input('phone');
                    $member->postal = $request->input('postal');
                    $member->save();

                    return redirect('profile')->with('success','Account editted');
                }else{
                    return redirect('profile')->with('error','Inaccurate credentials');
                }

            } catch (\Illuminate\Database\QueryException $e) {
                var_dump($e->errorInfo);
            }

        }else{
            return redirect('profile')->with('error','Inaccurate credentials');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MemberProfile  $memberProfile
     * @return \Illuminate\Http\Response
     */
    public function destroy(MemberProfile $memberProfile)
    {
        //
    }
}
