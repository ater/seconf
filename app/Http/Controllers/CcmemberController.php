<?php

namespace App\Http\Controllers;

use App\Contribution;
use App\LoanRequest;
use App\LoanType;
use App\Loan;
use App\LoanRepayment;
use Illuminate\Http\Request;

class CcmemberController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:ccmember');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ccmember.home');
    }
}
