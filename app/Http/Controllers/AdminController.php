<?php

namespace App\Http\Controllers;

use App\Contribution;
use App\LoanRequest;
use App\LoanType;
use App\Loan;
use App\LoanRepayment;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contributions = Contribution::where('status',2)->count();
        $contributions_by_date = Contribution::selectRaw("DATE(`created_at`) as  date, SUM(amount) as sum")->groupBy('date')->get();
        if($contributions_by_date){
            foreach($contributions_by_date as $contribution){
                $contribution_stats['dates'][] = $contribution["date"];
                $contribution_stats['amount'][] = intval($contribution["sum"]);
            }            
        }else{
            $contribution_stats = null;
        }
        $shares["value"] = Contribution::where('status', 2)->sum("amount");
        $shares["number"] = $shares["value"]/Contribution::valuePerShare;

        $loan_types = LoanType::all();
        $requests = LoanRequest::count();
        $requests_by_type = null;
        if($loan_types){
            foreach ($loan_types as $loan_type) {
                $requests_by_type["type"][] = $loan_type->title;
                $requests_by_type["number"][] = LoanRequest::where('loan_type_id',$loan_type->id)->count();
            }
        }
        $loans = Loan::count();

        return view('admin.home',compact('contributions','contribution_stats','shares','requests','requests_by_type','loans'));
    }
    public function posts()
    {
        return view('admin.posts');
    }
}
