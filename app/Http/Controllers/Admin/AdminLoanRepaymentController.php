<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\LoanRepayment;
use App\Loan;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

class AdminLoanRepaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $repayments =LoanRepayment::orderBy('created_at','desc')->paginate(10);
        $counts = null;
        if($repayments!=null){
            foreach (LoanRepayment::statuses as $key=> $status){
                $num= LoanRepayment::where('status',$key)->count();
                if($num){
                    $counts[$status] = $num;
                }else{
                    $counts[$status] =0;
                }
            } 
        }
        return view('admin.loans.repayments.index',compact('repayments','counts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $loans = Loan::where('status','2')->get();
        return view('admin.loans.repayments.create', compact('loans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'reference'=> 'required | unique:loan_repayments',
            'amount' => 'required | integer | min:1',
            'loan_reference' => 'required | integer | min:1'
        ]);
        $loan = Loan::where('loan_request_id',$request->input('loan_reference'))->first();
        if(!$loan){
            return back()->with("error","No Matching loan found");
        }
        try {            
            //Create Loan Repayment
            $repayment = new LoanRepayment;
            $repayment->id= Uuid::generate()->string;
            $repayment->reference = $request->input('reference');
            $repayment->amount = $request->input('amount');
            $repayment->loan_id = $loan->id;
            $repayment->save();

            return redirect(route('admin.loans.repayments.index'))->with('success','Repayment Added');

        } catch (\Illuminate\Database\QueryException $e) {
            var_dump($e->errorInfo);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LoanRepayment  $loanRepayment
     * @return \Illuminate\Http\Response
     */
    public function show($reference)
    {
        $repayment = LoanRepayment::where('reference',$reference)->first();
        return view('admin.loans.repayments.single', compact('repayment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LoanRepayment  $loanRepayment
     * @return \Illuminate\Http\Response
     */
    public function edit($reference)
    {
        $loans = Loan::where('status','2')->get();
        $statuses= LoanRepayment::statuses;
        $repayment = LoanRepayment::where('reference',$reference)->first();
        return view('admin.loans.repayments.edit', compact('repayment','loans','statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LoanRepayment  $loanRepayment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request,[
            'reference' => 'required | exists:loan_repayments',
            'amount' => 'required | integer | min:1',
            'loan_reference' => 'required | integer | exists:loans,loan_request_id',
            'status' => 'required | integer | min:0'
        ]);
        $loan = Loan::where('loan_request_id',$request->input('loan_reference'))->first();
        $repayment = LoanRepayment::where('reference',$request->input('reference'))->first();
        if(!$loan && !$repayment){
            return back()->with("error","No Matching loan found");
        }
        try {            
            //Edit Loan Repayment
            $repayment->amount = $request->input('amount');
            $repayment->loan_id = $loan->id;
            $repayment->status = $request->input('status');
            $repayment->save();

            return redirect(route('admin.loans.repayments.index'))->with('success','Repayment Edited');

        } catch (\Illuminate\Database\QueryException $e) {
            var_dump($e->errorInfo);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LoanRepayment  $loanRepayment
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoanRepayment $loanRepayment)
    {
        //
    }
}
