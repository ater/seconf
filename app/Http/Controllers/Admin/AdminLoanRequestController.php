<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\LoanRequest;
use App\LoanType;
use App\MemberProfile;
use App\Loan;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

class AdminLoanRequestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $loan_requests =LoanRequest::orderBy('created_at','desc')->paginate(10); 
        $counts =array();
        foreach (LoanRequest::statuses as $key=> $status){
            $num= LoanRequest::where('status',$key)->count();
            if($num){
                $counts[$status] = $num;
            }else{
                $counts[$status] =0;
            }
        }       
        return view('admin.loans.requests.index',compact('loan_requests','counts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $members = MemberProfile::all();
        $loan_types = LoanType::all();
        return view('admin.loans.requests.create', compact('loan_types','members'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'amount' => 'required | integer | min:1',
            'duration' => 'required | integer | min:1',
            'reason'=> 'required | string',
            'loan_type' => 'required | integer | min:1',
            'member_id' => 'required | integer | min:1'
        ]);

        try {            
            //Create Loan Request
            $loanRequest = new LoanRequest;
            $loanRequest->amount = $request->input('amount');
            $loanRequest->duration = $request->input('duration');
            $loanRequest->member_profile_id = $request->input('member_id');
            $loanRequest->reason = $request->input('reason');
            $loanRequest->loan_type_id = $request->input('loan_type');
            $loanRequest->save();

            return redirect(route('admin.loans.urequests.index'))->with('success','Loan Request Added');

        } catch (\Illuminate\Database\QueryException $e) {
            var_dump($e->errorInfo);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LoanRequest  $loanRequest
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $loan_request = LoanRequest::find($id);
        return view('admin.loans.requests.single',compact('loan_request'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LoanRequest  $loanRequest
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {        
        $loan_request = LoanRequest::find($id);
        if(!$loan_request->editable){
            return redirect(route('admin.loans.urequests.show',$id))->with("error","This is not edittable");
        }
        $members = MemberProfile::all();        
        $loan_types = LoanType::all();
        $statuses = LoanRequest::statuses;
        return view('admin.loans.requests.edit',compact('members','loan_types','loan_request', 'statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LoanRequest  $loanRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request,[
            'id' => 'required | integer | min:1',
            'amount' => 'required | integer | min:1',
            'duration' => 'required | integer | min:1',
            'reason'=> 'required | string',
            'loan_type' => 'required | integer | min:1',
            'member_id' => 'required | integer | min:1',
            'status' => 'required | integer | min:0'
        ]);

        try {            
            //Edit Loan Request
            $loanRequest = LoanRequest::find($request->input('id'));
            $loanRequest->amount = $request->input('amount');
            $loanRequest->duration = $request->input('duration');
            $loanRequest->member_profile_id = $request->input('member_id');
            $loanRequest->reason = $request->input('reason');
            $loanRequest->loan_type_id = $request->input('loan_type');
            $loanRequest->status = $request->input('status');
            $succesfully = $loanRequest->save();
            if(($request->input('status')==2)&& $succesfully){ //loan request approved and saved
                $loan = new Loan;
                $loan->loan_request_id = $request->input('id');
                $loan->amount = $request->input('amount');
                $loan->id = Uuid::generate()->string;
                $loan->save();
                return redirect(route('admin.loans.show',$request->input('id')))->with('success','Loan Added');
            }

            return redirect(route('admin.loans.urequests.index'))->with('success','Loan Request Editted');

        } catch (\Illuminate\Database\QueryException $e) {
            var_dump($e->errorInfo);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LoanRequest  $loanRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoanRequest $loanRequest)
    {
        //
    }
}
