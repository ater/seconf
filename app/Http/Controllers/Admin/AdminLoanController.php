<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Loan;
use App\LoanRequest;
use App\LoanRepayment;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

class AdminLoanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $loans =Loan::orderBy('created_at','desc')->paginate(10);
        $counts= null;
        foreach (Loan::statuses as $key=> $status){
            $num= Loan::where('status',$key)->count();
            if($num){
                $counts[$status] = $num;
            }else{
                $counts[$status] =0;
            }
        } 
        return view('admin.loans.index', compact('loans', 'counts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$loan = LoanRequest::find($id)->loan()->first();
        $loan = Loan::where('loan_request_id',$id)->first();
        $counts = null;
        $repayments = null;
        if($loan!=null){
            $repayments = $loan->repayments()->orderBy('created_at','desc')->paginate(10);
            foreach (LoanRepayment::statuses as $key=> $status){
                $num= LoanRepayment::where('loan_id',$loan->id)->where('loan_repayments.status',$key)->count();
                if($num){
                    $counts[$status] = $num;
                }else{
                    $counts[$status] =0;
                }
            } 
        }
        return view('admin.loans.single',compact('loan','repayments','counts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Loan $loan)
    {
        //
    }
    public function updateStatus(Request $request, $reference)
    {
        $this->validate($request,[
            'referenced'=> 'required | integer | min:1',
            'status'=> 'required | integer | min:0'
        ]);
        $referenced = $request->input('referenced');
        $status = $request->input('status');
        if($referenced!=$reference){
            return back()->with('error', "Inaccurate Infomation 1");
        }
        $loan = Loan::where('loan_request_id', $referenced)->first();
        
        if($loan && (count(Loan::statuses)>$status)){
            $loan->status = $status;
            $loan->save();
            return redirect(route("admin.loans.index"))->with("success", "Successfully Edited");
        }else{
            return back()->with('error', "Inaccurate Infomation");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Loan $loan)
    {
        //
    }
}
