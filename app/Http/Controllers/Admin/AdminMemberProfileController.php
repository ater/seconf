<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\MemberProfile;
use App\User;
use Illuminate\Http\Request;

class AdminMemberProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members =MemberProfile::orderBy('created_at','desc')->paginate(10);
        $counts = null;
        if($members!=null){
            foreach (MemberProfile::statuses as $key=> $status){
                $num= MemberProfile::where('status',$key)->count();
                if($num){
                    $counts[$status] = $num;
                }else{
                    $counts[$status] =0;
                }
            } 
        }
        return view('admin.members.index', compact('members','counts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=> 'required',
            'national'=> 'required',
            'phone'=> 'required',
            'email'=> 'required | email',
            'postal'=> 'required'
        ]);
        $user = User::where('email',$request->input('email'))->first();
        if($user){
                try {            
                //Create Member
                $member = new MemberProfile;
                $member->user_id = $user->id;
                $member->fullname = $request->input('name');
                $member->national_id = $request->input('national');
                $member->phone = $request->input('phone');
                $member->postal = $request->input('postal');
                $member->save();

                return redirect('admin/members')->with('success','Member Added');

            } catch (\Illuminate\Database\QueryException $e) {
                var_dump($e->errorInfo);
            }

        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MemberProfile  $memberProfile
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $member = MemberProfile::find($id);
        return view('admin.members.single')->with('member',$member);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MemberProfile  $memberProfile
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = MemberProfile::find($id);
        $statuses = MemberProfile::statuses;
        return view('admin.members.edit', compact('member','statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MemberProfile  $memberProfile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request,[
            'name'=> 'required',
            'national'=> 'required',
            'phone'=> 'required',
            'email'=> 'required | email',
            'postal'=> 'required',
            'member_num'=> 'required | integer | min:1',
            'status'=> 'required'
        ]);
        $user = User::where('email',$request->input('email'))->first();
        $member = MemberProfile::find($request->input('member_num'));
        if($user && $user->profile->id == $request->input('member_num') && $member->user_id == $user->id){
            try {            
                $member->fullname = $request->input('name');
                $member->national_id = $request->input('national');
                $member->phone = $request->input('phone');
                $member->postal = $request->input('postal');
                $member->status = $request->input('status');
                $member->save();

                return redirect(route('admin.members.index'))->with('success','Member Edited');

            } catch (\Illuminate\Database\QueryException $e) {
                var_dump($e->errorInfo);
            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MemberProfile  $memberProfile
     * @return \Illuminate\Http\Response
     */
    public function destroy(MemberProfile $memberProfile)
    {
        //
    }
}
