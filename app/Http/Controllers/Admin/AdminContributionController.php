<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Contribution;
use App\MemberProfile;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

class AdminContributionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contributions =Contribution::orderBy('created_at','desc')->paginate(10);
        $counts =array();
        foreach (Contribution::statuses as $key=> $status){
            $num = Contribution::where('status',$key)->count();
            if($num){
                $counts[$status] = $num;
            }else{
                $counts[$status] =0;
            }
        }
        $shares = array();
        $shares["value"] = Contribution::where('status', 2)->sum("amount");
        $shares["number"] = $shares["value"]/Contribution::valuePerShare;
        return view('admin.contributions.index', compact('contributions','counts','shares'));
    }
    /**
     * Once 
     */
    public function statusCategory($status)
    {
        $status =ucfirst($status);
        $index = array_search($status,Contribution::statuses); //make sure the status is a valid model status
        
        if($index){ //index 0 will be read as false therfore deleted ones will not be read for users
            $contributions = Contribution::where('status', $index)->orderBy('created_at','desc')->paginate(10);
            
        }else{
            $contributions = null;
        }
        return view('admin.contributions.category', compact('contributions', 'status'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $members = MemberProfile::all();
        return view('admin.contributions.create', compact('members'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'reference'=> 'required | unique:contributions',
            'amount' => 'required | integer | min:1',
            'member_id' => 'required | integer | min:1'
        ]);

        try {            
            //Create Contribution
            $contribution = new Contribution;
            $contribution->id= Uuid::generate()->string;
            $contribution->reference = $request->input('reference');
            $contribution->amount = $request->input('amount');
            $contribution->member_profile_id = $request->input('member_id');
            $contribution->status = 2;
            $contribution->save();

            return redirect(route('admin.contributions.index'))->with('success','Contribution Added');

        } catch (\Illuminate\Database\QueryException $e) {
            var_dump($e->errorInfo);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contribution  $contribution
     * @return \Illuminate\Http\Response
     */
    public function show($ref)
    {   
        $contribution = Contribution::where('reference',$ref)->first();
        //return $contribution;
        //return var_dump($contribution);
        return view('admin.contributions.single')->with('contribution',$contribution);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contribution  $contribution
     * @return \Illuminate\Http\Response
     */
    public function edit($ref)
    {
        $contribution = Contribution::where('reference',$ref)->first();
        $members = MemberProfile::all();
        $statuses = Contribution::statuses;
        return view('admin.contributions.edit', compact('contribution','members','statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contribution  $contribution
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request,[
            'reference'=> 'required',
            'amount' => 'required | integer | min:1',
            'member_id' => 'required | integer | min:1',
            'status' => 'required | integer | min:0'
        ]);

        try {            
            //Find Contribution
            $contribution = Contribution::where('reference',$request->input('reference'))->first();
            $contribution->amount = $request->input('amount');
            $contribution->member_profile_id = $request->input('member_id');
            $contribution->status = $request->input('status');
            $contribution->save();

            return redirect(route('admin.contributions.index'))->with('success','Contribution Editted');

        } catch (\Illuminate\Database\QueryException $e) {
            var_dump($e->errorInfo);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contribution  $contribution
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contribution $contribution)
    {
        //
    }
}
