<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AdminLoginController extends Controller
{
    //set up middleware
    public function __construct(){
        $this->middleware('guest:admin');
    }
    //
    public function showLoginForm(){
        return view('auth.admin.login');
    }
    
    public function login(Request $request){
        //validate form data
        $this->validate($request, [
            'email'=> 'required|email',
            'password'=> 'required'
        ]);
        //Attempt to login user        $credentials is the array of credentials
        $credentials = ['email'=>$request->email, 'password'=>$request->password];
        //if successful, redirect to intended location appropriately
        if(Auth::guard('admin')->attempt($credentials, $request->remember)){
            return redirect()->intended(route('admin.dashboard'));
        }

        //else if unsuccessful, redirect back to login form with pre-filled data
        return redirect()->back()->withInput($request->only('email','remember'));
    }
}
