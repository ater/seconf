<?php

namespace App\Http\Controllers;

use App\LoanRequestCCFeedback;
use Illuminate\Http\Request;

class LoanRequestCCFeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LoanRequestCCFeedback  $loanRequestCCFeedback
     * @return \Illuminate\Http\Response
     */
    public function show(LoanRequestCCFeedback $loanRequestCCFeedback)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LoanRequestCCFeedback  $loanRequestCCFeedback
     * @return \Illuminate\Http\Response
     */
    public function edit(LoanRequestCCFeedback $loanRequestCCFeedback)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LoanRequestCCFeedback  $loanRequestCCFeedback
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LoanRequestCCFeedback $loanRequestCCFeedback)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LoanRequestCCFeedback  $loanRequestCCFeedback
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoanRequestCCFeedback $loanRequestCCFeedback)
    {
        //
    }
}
