<?php

namespace App\Http\Controllers;

use App\Contribution;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = auth()->user()->profile;
        $contributions=0;
        $shares= array("value"=>0,"number"=>0);
        $requests = 0;
        $loans = 0;
        if($profile){
            $id= $profile->id;
            $contributions = Contribution::where('member_profile_id',$id)->where('status',2)->count();
            $shares["value"] = Contribution::where('member_profile_id',$id)->where('status', 2)->sum("amount");
            $shares["number"] = $shares["value"]/20;
            $requests = $profile->loan_requests()->count();
            $loans = $profile->loans()->count();
        }
        return view('home', compact('contributions','shares','requests','loans'));
    }
    public function noProfile()
    {
        return view('noProfile');
    }
}
