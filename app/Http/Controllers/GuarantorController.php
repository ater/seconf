<?php

namespace App\Http\Controllers;

use App\Guarantor;
use App\LoanRequest;
use App\MemberProfile;
use Illuminate\Http\Request;

class GuarantorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($loan_ref)
    {
        if(auth()->user()->profile){    
            $id= auth()->user()->profile->id;
            $urequest = LoanRequest::find($loan_ref);                        
            if($urequest->member_profile_id == $id){ //If the owner is querying, simply look for the guarantors using the loan reference
                $guarantors =Guarantor::where('loan_request_id',$loan_ref)->orderBy('created_at','desc')->paginate(10);
            }else{ //if it is a valid user, select where they are a guarantor of the loan
                $guarantors =Guarantor::where('loan_request_id',$loan_ref)->where('member_profile_id',$id)->orderBy('created_at','desc')->paginate(10);
            }            
            return view('loans.guarantors.index', compact('guarantors','counts', 'value_guaranted','loan_ref'));
        }
        else{
            return redirect('/40');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($loan_ref)
    {
        if(auth()->user()->profile){
            $id= auth()->user()->profile->id;
            $urequest = LoanRequest::find($loan_ref);                        
            if($urequest && $urequest->member_profile_id == $id && $urequest->guarantorRequired){
                $members = MemberProfile::where('status', 2)->get(['id','fullname']);
                return view('loans.guarantors.create', compact('members','loan_ref'));
            }else{
                return redirect()->route('loans.urequests.show',$loan_ref)->with('error','Guarantor not required for this type of loan');
            }
        }else{
            return redirect('/40');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($loan_ref,Request $request)
    {
        if(auth()->user()->profile){
            $this->validate($request,[
                'member_id'=> 'required | integer | exists:member_profiles,id',
                'amount' => 'required | integer | min:0'
            ]);
            $id= auth()->user()->profile->id;
            $urequest = LoanRequest::find($loan_ref);                        
            if($urequest && $urequest->member_profile_id == $id && $urequest->guarantorRequired){ //if request exists, belongs to the user attempting to add a guarantor and loan type requires guarantor
                if($urequest->member_profile_id!=$request->input('member_id')){ //if user isn't attempting to add themselves as guarntor
                    $already_guarantor = $urequest->guarantors()->where('member_profile_id',$request->input('member_id'))->first(); //find if a guarantor entry already exists
                    if($already_guarantor){
                        return redirect()->route('loans.guarantors.edit',[$loan_ref,$already_guarantor->id])->with(['error'=>'You already have this guarantor for this loan, kindly update the amount and save rather than trying to add a new entry.']);
                    }
                    $guarantor = new Guarantor;
                    $guarantor->member_profile_id = $request->input('member_id');
                    $guarantor->loan_request_id = $loan_ref;
                    $guarantor->amount = $request->input('amount');
                    if($guarantor->save()){
                        if($urequest->status!=11){
                            $urequest->status = 11;
                            $urequest->save();
                        }
                        return redirect()->route('loans.urequests.show',$loan_ref)->with("success","Guarantor Successfully added");
                    }else{
                        return back()->with("error","Well this is embarassing");
                    }
                }else{
                    return back()->with("error","You can't be your own guarantor");
                }
            }else{
                return redirect()->route('loans.urequests.show',$loan_ref)->with('error','Guarantor not required for this type of loan');
            }
        }else{
            return redirect('/40');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Guarantor  $guarantor
     * @return \Illuminate\Http\Response
     */
    public function show(Guarantor $guarantor)
    {
        //
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function guarantees()
    {
        if(auth()->user()->profile){    
            $id= auth()->user()->profile->id;
            $guarantees =Guarantor::where('member_profile_id',$id)->orderBy('created_at','desc')->paginate(10);           
            foreach (Guarantor::statuses as $key=> $status){
                $num = Guarantor::where('member_profile_id',$id)->where('status',$key)->count();
                if($num){
                    $counts[$status] = $num;
                }else{
                    $counts[$status] =0;
                }
            } 
            return view('loans.guarantees.index', compact('guarantees','counts'));
        }
        else{
            return "Hey";
            return redirect('/40');
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Guarantor  $guarantor
     * @return \Illuminate\Http\Response
     */
    public function edit($loan_ref, $guarantor_ref)
    {
        if(auth()->user()->profile){
            $id= auth()->user()->profile->id;
            $urequest = LoanRequest::find($loan_ref);                        
            if($urequest && $urequest->member_profile_id == $id && $urequest->guarantorRequired){
                $guarantor = $urequest->guarantors()->find($guarantor_ref); //find if a guarantor entry already exists                
                return view('loans.guarantors.edit', compact('guarantor','loan_ref'));
            }else{
                return redirect()->route('loans.urequests.show',$loan_ref)->with('error','Guarantor not required for this type of loan');
            }
        }else{
            return redirect('/40');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Guarantor  $guarantor
     * @return \Illuminate\Http\Response
     */
    public function update($loan_ref, $guarantor_ref, Request $request)
    {
        if(auth()->user()->profile){
            $this->validate($request,[
                'amount' => 'required | integer | min:1'
            ]);
            $id= auth()->user()->profile->id;
            $guarantor = Guarantor::find($guarantor_ref);                        
            if($guarantor && $guarantor->loan_request->member_profile_id == $id){                
                $guarantor->amount = $request->input('amount');
                if($guarantor->save()){
                    return redirect()->route('loans.urequests.show',$loan_ref)->with("success","Guarantor Successfully editted");
                }else{
                    return back()->with("error","Well this is embarassing");
                }                
            }else{
                return redirect()->route('loans.urequests.show',$loan_ref)->with('error','Guarantor not required for this type of loan');
            }
        }else{
            return redirect('/40');
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Guarantor  $guarantor
     * @return \Illuminate\Http\Response
     */
    public function approve($guarantor_ref, Request $request)
    {
        if(auth()->user()->profile){
            $this->validate($request,[
                'status' => 'required | integer | min:1'
            ]);
            $id= auth()->user()->profile->id;
            $guarantor = Guarantor::find($guarantor_ref);
            if($guarantor && $guarantor->member_profile_id == $id){                
                $guarantor->status = $request->input('status');
                if($guarantor->save()){
                    return redirect()->route('loans.guarantees.index')->with("success","Guarantor Successfully editted");
                }else{
                    return back()->with("error","Well this is embarassing");
                }                
            }else{
                return redirect()->route('loans.guarantees')->with('error','Action Not allowed');
            }
        }else{
            return redirect('/40');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Guarantor  $guarantor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Guarantor $guarantor)
    {
        //
    }
}
