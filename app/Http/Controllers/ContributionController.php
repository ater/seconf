<?php

namespace App\Http\Controllers;

use App\Contribution;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

class ContributionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->profile){
            $id= auth()->user()->profile->id;
            $contributions =Contribution::where('member_profile_id',$id)->orderBy('created_at','desc')->paginate(10);
            $counts =array();
            foreach (Contribution::statuses as $key=> $status){
                $num = Contribution::where('member_profile_id',$id)->where('status',$key)->count();
                if($num){
                    $counts[$status] = $num;
                }else{
                    $counts[$status] =0;
                }
            }
            $shares = array();
            $shares["value"] = Contribution::where('member_profile_id',$id)->where('status', 2)->sum("amount");
            $shares["number"] = $shares["value"]/Contribution::valuePerShare;
            return view('contributions.index', compact('contributions','counts', 'shares'));
            //return $contributions;
        }
        else{
            return redirect('/40');
        }
    }

    /**
     * Once 
     */
    public function statusCategory($status)
    {
        $status =ucfirst($status);
        $index = array_search($status,Contribution::statuses); //make sure the status is a valid model status
        
        $id= auth()->user()->profile->id;
        if($index){ //index 0 will be read as false therfore deleted ones will not be read for users
            $contributions = Contribution::where('member_profile_id',$id)->where('status', $index)->orderBy('created_at','desc')->paginate(10);
            
        }else{
            $contributions = null;
        }
        return view('contributions.category', compact('contributions', 'status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contributions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'reference'=> 'required | unique:contributions',
            'amount' => 'required | integer | min:1'
        ]);

        try {            
            //Create Contribution
            $contribution = new Contribution;
            $contribution->id= Uuid::generate()->string;
            $contribution->reference = $request->input('reference');
            $contribution->amount = $request->input('amount');
            $contribution->member_profile_id = auth()->user()->profile->id;
            $contribution->save();

            return redirect('/contributions')->with('success','Contribution Added');

        } catch (\Illuminate\Database\QueryException $e) {
            var_dump($e->errorInfo);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contribution  $contribution
     * @return \Illuminate\Http\Response
     */
    public function show($ref)
    {   
        $contribution = Contribution::where('member_profile_id',auth()->user()->profile->id)->where('reference',$ref)->first();
        //return $contribution;
        //return var_dump($contribution);
        return view('contributions.single')->with('contribution',$contribution);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contribution  $contribution
     * @return \Illuminate\Http\Response
     */
    public function edit(Contribution $contribution)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contribution  $contribution
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contribution $contribution)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contribution  $contribution
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contribution $contribution)
    {
        //
    }

    private function getStatus($status){
        $statusText = "Pending";
        switch ($status) {
            case 0:
                $statusText = "Deleted";
                break;
            
            case 1:
                $statusText = "Pending";
                break;
            
            case 2:
                $statusText = "Confirmed";
                break;
            
            case 3:
                $statusText = "With Issue";
                break;
            
            default:
                $statusText = "Pending";
                break;
        }
        return $statusText;
    }
}
