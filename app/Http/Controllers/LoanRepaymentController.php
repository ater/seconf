<?php

namespace App\Http\Controllers;

use App\LoanRepayment;
use App\Loan;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

class LoanRepaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($loan_ref)
    {
        //$repayments =auth()->user()->profile->loans()->loan_repayments()->orderBy('created_at','desc')->paginate(10);     
        $loan =auth()->user()->profile->loan_requests()->find($loan_ref)->loan;
        $repayments;
        if($loan){
            $repayments = $loan->repayments()->paginate(10);
            $counts =array();
            foreach (LoanRepayment::statuses as $key=> $status){
                $num= $loan->repayments()->where('status',$key)->count();
                if($num){
                    $counts[$status] = $num;
                }else{
                    $counts[$status] =0;
                }
            }  
        }
        return view('loans.repayments.index',compact('repayments','loan_ref','counts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($loan_ref)
    {
        $loan = auth()->user()->profile->loans()->where('loans.loan_request_id',$loan_ref)->where('loans.status',2)->get();
        if($loan){
            return view('loans.repayments.create', compact('loan_ref'));
        }else{
            return redirect()->route('loans.index')->with("error",'No loan Found');
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($loan_ref,Request $request)
    {
        $this->validate($request,[
            'reference'=> 'required | unique:loan_repayments',
            'amount' => 'required | integer | min:1'
        ]);
        $loan = Loan::where('loan_request_id',$loan_ref)->first();
        if(!$loan){
            return back()->with("error","No Matching loan found");
        }
        try {            
            //Create Loan Repayment
            $repayment = new LoanRepayment;
            $repayment->id= Uuid::generate()->string;
            $repayment->reference = $request->input('reference');
            $repayment->amount = $request->input('amount');
            $repayment->loan_id = $loan->id;
            $repayment->save();

            return redirect()->route('loans.show',$loan_ref)->with('success','Repayment Added');

        } catch (\Illuminate\Database\QueryException $e) {
            var_dump($e->errorInfo);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LoanRepayment  $loanRepayment
     * @return \Illuminate\Http\Response
     */
    public function show($loan_ref,$reference)
    {
        $repayment = LoanRepayment::where('reference',$reference)->first();
        if($repayment!==null){        
            $loan_request = $repayment->loan()->first()->loan_request()->first();
            
            if((string) auth()->user()->profile->id!==$loan_request->member_profile_id){
                $repayment = null;
            }
        }
        return view('loans.repayments.single', compact('repayment','loan_ref'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LoanRepayment  $loanRepayment
     * @return \Illuminate\Http\Response
     */
    public function edit(LoanRepayment $loanRepayment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LoanRepayment  $loanRepayment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LoanRepayment $loanRepayment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LoanRepayment  $loanRepayment
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoanRepayment $loanRepayment)
    {
        //
    }
}
