<?php

namespace App\Http\Controllers\CCMember;

use App\Http\Controllers\Controller;
use App\LoanRequest;
use App\LoanType;
use App\MemberProfile;
use App\Guarantor;
use App\Loan;
use App\LoanRequestCCFeedback;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

class CCMLoanRequestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:ccmember');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $loan_requests =LoanRequest::orderBy('created_at','desc')->paginate(10); 
        $counts =array();
        foreach (LoanRequest::statuses as $key=> $status){
            $num= LoanRequest::where('status',$key)->count();
            if($num){
                $counts[$status] = $num;
            }else{
                $counts[$status] =0;
            }
        }       
        return view('ccmember.loans.requests.index',compact('loan_requests','counts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->route('ccm.loans.urequests.index')->with('error','Commitee Account can only view, approve or reject Requests');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return redirect()->route('ccmember.loans.urequests.index')->with('error','Commitee Account can only view, approve or reject Requests');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LoanRequest  $loanRequest
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $loan_request = LoanRequest::find($id);
        $guarantors = null;
        $counts =array();
        if($loan_request){
            $guarantors =Guarantor::where('loan_request_id',$id)->orderBy('created_at','desc')->paginate(10);
            $cc_feedback =LoanRequestCCFeedback::where('loan_request_id',$id)->orderBy('created_at','desc')->paginate(10);
            $ccmember_has_given_feedback = auth()->user()->loan_requests_feedback()->where('loan_request_id','=',$id)->first();
            //return $ccmember_has_given_feedback;
            foreach (Guarantor::statuses as $key=> $status){
                $num= Guarantor::where('loan_request_id',$id)->where('status',$key)->count();
                if($num){
                    $counts[$status] = $num;
                }else{
                    $counts[$status] =0;
                }
            }
            $total_guaranteed = $loan_request->guarantors()->where('status','=',2)->sum('amount');
            // return $total_guarantable;
        }  
        return view('ccmember.loans.requests.single',compact('loan_request','guarantors','counts', 'total_guaranteed','cc_feedback','ccmember_has_given_feedback'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LoanRequest  $loanRequest
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {        
        return redirect()->route('ccmember.loans.urequests.index')->with('error','Commitee Account can only view, approve or reject Requests');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LoanRequest  $loanRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $loan_ref)
    {
        $this->validate($request,[
            'id' => 'required | integer | min:1 | exists:loan_requests',
            'status' => 'required | integer | min:0'
        ]);

        try {                        
            //Edit Loan Request
            $loan_request = LoanRequest::find($request->input('id'));
            if($loan_request->cc_feedback()->where('status','=',2)->count()>=LoanRequestCCFeedback::feedbackNeededForApproval){
                $loan_request->status = $request->input('status');
                if($loan_request->save()){
                    $loan = new Loan;
                    $loan->loan_request_id = $loan_request->id;
                    $loan->amount = $loan_request->amount;
                    $loan->id = Uuid::generate()->string;
                    $loan->save();
                    return redirect()->route('ccmember.loans.urequests.show',$request->input('id'))->with('success','Loan Request Approved');
                }else{
                    return redirect()->route('ccmember.loans.urequests.show',$request->input('id'))->with('error','An error occurred. Kindly, Try again');
                }
            }else{
                return redirect()->route('ccmember.loans.urequests.show',$request->input('id'))->with('error','Minimum number of approvals not met.');
            }


        } catch (\Illuminate\Database\QueryException $e) {
            var_dump($e->errorInfo);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LoanRequest  $loanRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoanRequest $loanRequest)
    {
        //
    }
}
