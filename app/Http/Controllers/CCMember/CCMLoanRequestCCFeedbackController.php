<?php


namespace App\Http\Controllers\CCMember;

use App\Http\Controllers\Controller;
use App\LoanRequestCCFeedback;
use Illuminate\Http\Request;

class CCMLoanRequestCCFeedbackController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:ccmember');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $loan_ref)
    {
        $this->validate($request,[
            'loan_ref'=> 'required | exists:loan_requests,id',
            'status' => 'required | integer | min:1'
        ]);

        try {            
            //Create FeedBack
            $cc_feedback = new LoanRequestCCFeedback;
            $cc_feedback->loan_request_id = $request->input('loan_ref');
            $cc_feedback->ccmember_id = auth()->user()->id;
            $cc_feedback->status = $request->input('status');
            $cc_feedback->save();

            return redirect()->route('ccmember.loans.urequests.index')->with('success','Feedback Added');

        } catch (\Illuminate\Database\QueryException $e) {
            var_dump($e->errorInfo);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LoanRequestCCFeedback  $loanRequestCCFeedback
     * @return \Illuminate\Http\Response
     */
    public function show(LoanRequestCCFeedback $loanRequestCCFeedback)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LoanRequestCCFeedback  $loanRequestCCFeedback
     * @return \Illuminate\Http\Response
     */
    public function edit(LoanRequestCCFeedback $loanRequestCCFeedback)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LoanRequestCCFeedback  $loanRequestCCFeedback
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$loan_ref, $id)
    {
        $this->validate($request,[
            'loan_ref'=> 'required | exists:loan_requests,id',
            'status' => 'required | integer | min:1'
        ]);

        try {            
            //Create Contribution
            $cc_feedback = LoanRequestCCFeedback::find($id);
            if($cc_feedback){
                $cc_feedback->loan_request_id = $request->input('loan_ref');
                $cc_feedback->ccmember_id = auth()->user()->id;
                $cc_feedback->status = $request->input('status');
                $cc_feedback->save();

                return redirect()->route('ccmember.loans.urequests.show',$loan_ref)->with('success','Feedback Editted');
            }
            return redirect()->route('ccmember.loans.urequests.index',$loan_ref)->with('error','Requested Feedback Cannot be accessed.');

        } catch (\Illuminate\Database\QueryException $e) {
            var_dump($e->errorInfo);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LoanRequestCCFeedback  $loanRequestCCFeedback
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoanRequestCCFeedback $loanRequestCCFeedback)
    {
        //
    }
}
