<?php

namespace App\Http\Controllers;

use App\Loan;
use App\LoanRepayment;
use Illuminate\Http\Request;

class LoanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = auth()->user()->profile;
        if($profile){
            $id= $profile->id;
            $loans = $profile->loans()->paginate(10);
            foreach (Loan::statuses as $key=> $status){
                $num= $profile->loans()->where('loans.status',$key)->count();
                if($num){
                    $counts[$status] = $num;
                }else{
                    $counts[$status] =0;
                }
            }                  
            
            return view('loans.index',compact('loans','counts'));
        }
        else{
            return redirect('/40');
        }
        //$loans =Loan::orderBy('created_at','desc')->paginate(10);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function show($ref)
    {
        //$loan = auth()->user()->profile->loanRequests()->find($ref)->loan;
        $loan = auth()->user()->profile->loans()->where("loan_request_id",$ref)->first();
        $repayments = null;
        $counts = null;
        if($loan!=null){
            $repayments = $loan->repayments()->orderBy('created_at','desc')->paginate(10);
            foreach (LoanRepayment::statuses as $key=> $status){
                $num= LoanRepayment::where('loan_id',$loan->id)->where('loan_repayments.status',$key)->count();
                if($num){
                    $counts[$status] = $num;
                }else{
                    $counts[$status] =0;
                }
            } 
        }
        return view('loans.single', compact('loan','repayments','counts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function edit(Loan $loan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Loan $loan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Loan $loan)
    {
        //
    }
}
