<?php

namespace App\Http\Controllers;

use App\LoanRequest;
use App\LoanType;
use App\Guarantor;
use Illuminate\Http\Request;

class LoanRequestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = auth()->user()->profile;
        if($profile){
            $id= $profile->id;
            $loan_requests =LoanRequest::where('member_profile_id',$id)->orderBy('created_at','desc')->paginate(10);
            $counts =array();
            foreach (LoanRequest::statuses as $key=> $status){
                $num= LoanRequest::where('member_profile_id',$id)->where('status',$key)->count();
                if($num){
                    $counts[$status] = $num;
                }else{
                    $counts[$status] =0;
                }
            }
            return view('loans.requests.index',compact('loan_requests', 'counts'));
        }
        else{
            return redirect('/40');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $loan_types = LoanType::all();
        return view('loans.requests.create', compact('loan_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'amount' => 'required | integer',
            'duration' => 'required | integer | min:1 | max:24 ',
            'reason'=> 'required | string',
            'loan_type' => 'required | exists:loan_types,id'
        ]);

        try {            
            //Create Loan Request
            $loanRequest = new LoanRequest;
            $loanRequest->amount = $request->input('amount');
            $loanRequest->duration = $request->input('duration');
            $loanRequest->member_profile_id = auth()->user()->profile->id;
            $loanRequest->reason = $request->input('reason');
            $loanRequest->loan_type_id = $request->input('loan_type');
            if(LoanType::find($loanRequest->loan_type_id)->guarantorRequired){ //check if loan type requires guarantor
                $loanRequest->status = 10;
            }
            $loanRequest->save();

            return redirect(route('loans.index'))->with('success','Loan Requested');

        } catch (\Illuminate\Database\QueryException $e) {
            var_dump($e->errorInfo);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LoanRequest  $loanRequest
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $loan_request = auth()->user()->profile->loan_requests()->find($id);
        $guarantors = null;
        $counts =array();
        if($loan_request){
            $guarantors =Guarantor::where('loan_request_id',$id)->orderBy('created_at','desc')->paginate(10);
            foreach (Guarantor::statuses as $key=> $status){
                $num= Guarantor::where('loan_request_id',$id)->where('status',$key)->count();
                if($num){
                    $counts[$status] = $num;
                }else{
                    $counts[$status] =0;
                }
            }
            $total_guaranteed = $loan_request->guarantors()->where('status','=',2)->sum('amount');
            // return $total_guarantable;
        }  
        return view('loans.requests.single',compact('loan_request','guarantors','counts', 'total_guaranteed'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LoanRequest  $loanRequest
     * @return \Illuminate\Http\Response
     */
    public function sendToCommittee($loan_ref, Request $request)
    {
        if(auth()->user()->profile){
            $this->validate($request,[
                'status' => 'required | integer | min:11'
            ]);
            $urequest= auth()->user()->profile->loan_requests()->find($loan_ref);                
            if($urequest){ 
                $urequest->status = 12;
                $urequest->save();
                return redirect()->route('loans.urequests.show',$loan_ref)->with('success','Forwarded to Credit Committee');
            }else{
                return redirect()->route('loans.urequests.index')->with('error','Could not complete action');
            }
        }else{
            return redirect('/40');
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LoanRequest  $loanRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(LoanRequest $loanRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LoanRequest  $loanRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LoanRequest $loanRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LoanRequest  $loanRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoanRequest $loanRequest)
    {
        //
    }
}
