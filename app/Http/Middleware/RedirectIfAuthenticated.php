<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        switch ($guard) {
            case 'admin': //trying to access an admin level path
                if(Auth::guard($guard)->check()){ //are you logged in as an admin?
                    return redirect()->intended(route('admin.dashboard'));
                }
                break;
            
            case 'ccmember': //trying to access an ccmember level path
                if(Auth::guard($guard)->check()){ //are you logged in as an ccmember?
                    return redirect()->intended(route('ccmember.dashboard'));
                }
                break;
            
            default:
                if(Auth::guard($guard)->check()){ //are you logged in as default user?
                    return redirect()->intended(route('home'));
                }
                break;
        }
        // if (Auth::guard($guard)->check()) {
        //     return redirect('/home');
        // }

        return $next($request);
    }
}
