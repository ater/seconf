<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanRepayment extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';

    public function loan(){
        return $this->belongsTo('App\Loan');
    }
    
    public const statuses = ['Deleted','Pending','Approved','Rejected'];
    
    public function getStatusTextAttribute(){
        return $this::statuses[$this->status];
    }
}
