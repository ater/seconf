<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanType extends Model
{
    //public $table = "loan_types";
    public function getGuarantorRequiredAttribute(){
        return $this->id==1;
    }
    public function loan_requests(){
        return $this->hasMany('App\LoanRequest');
    }
}
