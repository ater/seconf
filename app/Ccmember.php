<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\AdminResetPasswordNotification;

class CCMember extends Authenticatable
{
    use Notifiable;
    public $table = "ccmembers";
    protected $guard = 'ccmember';
    public $incrementing = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function loan_requests_feedback(){
        return $this->hasMany('App\LoanRequestCCFeedback','ccmember_id');
    }
}
