<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guarantor extends Model
{
    //
    public function loan_request(){
        return $this->belongsTo('App\LoanRequest');
    }
    public function member_profile(){
        return $this->belongsTo('App\MemberProfile');
    }

    public const statuses = ['Deleted','Requested','Accepted','Rejected','Completed'];
    
    public function getStatusTextAttribute(){
        return $this::statuses[$this->status];
    }
    public function getEditableAttribute(){
        return $this->status>0 && $this->status!=2;
    }
    public function getDeletableAttribute(){
        return $this->status>0;
    }
}
