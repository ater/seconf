<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contribution extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';
    public const valuePerShare = 20; //Kshs. 20 is the set value of the share

    public function member_profile(){
        return $this->belongsTo('App\MemberProfile');
    }

    public const statuses = ['Deleted','Pending','Approved','Rejected'];
    
    public function getStatusTextAttribute(){
        return $this::statuses[$this->status];
    }
}
