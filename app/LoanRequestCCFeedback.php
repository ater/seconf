<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanRequestCCFeedback extends Model
{
    public function loan_request(){
        return $this->belongsTo('App\LoanRequest');
    }
    public function ccmember(){
        return $this->belongsTo('App\CCMember');
    }
    public const statuses = [0=>'Deleted',1=>'Pending With Issue',2=>'Approved',3=>'Rejected'];
    public const feedbackNeededForApproval = 1;
    public function getStatusTextAttribute(){
        return $this::statuses[$this->status];
    }
    public function getEditableAttribute(){
        return $this->status>0 && $this->status!=2;
    }
    public function getDeletableAttribute(){
        return $this->status>0;
    }
}
