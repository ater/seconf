<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complaints extends Model
{    
    public function member_profile(){
        return $this->belongsTo('App\MemberProfile');
    }
}
