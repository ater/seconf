<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    public $incrementing = false;
    public function loan_request(){
        return $this->belongsTo('App\LoanRequest');
    }
    public function repayments(){
        return $this->hasMany('App\LoanRepayment');
    }
    public const statuses = ['Deleted','Pending','Active','With Issue', 'Completed'];
    
    public function getStatusTextAttribute(){
        return $this::statuses[$this->status];
    }
}
