<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberProfile extends Model
{   
    public function user(){
        return $this->belongsTo('App\User');
    } 
    public function contributions(){
        return $this->hasMany('App\Contribution');
    }
    public function loans(){
        return $this->hasManyThrough('App\Loan','App\LoanRequest');
    }
    public function loan_requests(){
        return $this->hasMany('App\LoanRequest');
    }
    public function guarantees(){
        return $this->hasMany('App\Guarantor');
    }
    public function loan_repayments(){
        return $this->hasMany('App\LoanRepayment');
        //return $this->hasManyThrough('App\LoanRepayment','App\Loan');
    }
    public function complaints(){
        return $this->hasMany('App\Complaints');
    }
    
    public const statuses = ['Deleted','Suspended','Active'];
    
    public function getStatusTextAttribute(){
        return $this::statuses[$this->status];
    }
}
