<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/semantic.min.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/semantic.min.css') }}">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/components/icon.min.css" rel="stylesheet">
</head>
<body>
    <div id="sacco-main-nav-side-menu" class="ui sidebar inverted vertical menu" style="padding-top:100px;">
        @auth('admin')
            <a class="item" href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }}</a>    
            <a class="item" href="{{ route('admin.users.index') }}">{{ __('Users') }}</a>    
            <a class="item" href="{{ route('admin.members.index') }}">{{ __('Members') }}</a>    
            <a class="item" href="{{ route('admin.contributions.index') }}">{{ __('Contributions') }}</a>    
            <a class="item" href="{{ route('admin.loans.urequests.index') }}">{{ __('Loan Requests') }}</a>    
            <a class="item" href="{{ route('admin.loans.index') }}">{{ __('Loans') }}</a>    
            <a class="item" href="{{ route('admin.loans.repayments.index') }}">{{ __('Loan Repayments') }}</a>    
        @endauth 
        @auth('web')
            <a class="item" href="{{ route('home') }}">{{ __('Dashboard') }}</a>
            <a class="item" href="{{ route('contributions.index') }}">{{ __('Contributions') }}</a>
            <a class="item" href="{{ route('loans.index') }}">{{ __('Loans') }}</a>
            <a class="item" href="{{ route('loans.urequests.index') }}">{{ __('Loan Requests') }}</a>
            <a class="item" href="{{ route('loans.guarantees.index') }}">{{ __('Guarantees') }}</a>
        @endauth     
        @auth('ccmember')
            <a class="item" href="{{ route('ccmember.dashboard') }}">{{ __('Credit Dashboard') }}</a>
            <a class="item" href="{{ route('ccmember.loans.urequests.index') }}">{{ __('Loan Requests') }}</a>    
        @endauth            
    </div>
    <div class="pusher" id="app2">
        <div class="ui small menu">
                <a id="" class="active item sacco-main-nav-side-menu">
                    <i class="bars icon"></i>
                </a> 
                <a class="item" href="{{ url('/') }}">
                    {{ config('app.name', 'Sacco') }}
                </a>
                <div class="right menu">
                    @guest
                        <a class="item" href="{{ route('login') }}">{{ __('Login') }}</a>
                        <a class="item" href="{{ route('register') }}">{{ __('Register') }}</a>
                    @else          
                        @auth('admin')              
                            <a class="item" href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }}</a>
                        @endauth        
                        @auth('web')              
                            <a class="item" href="{{ route('home') }}">{{ __('Home') }}</a>
                        @endauth
                        <div class="ui dropdown item">
                            My Account
                            <div class="menu">
                                @auth('web')
                                    <a class="ui dropdown item" href="{{ route('profile.index') }}">{{ __('My Profile') }}<i class="dropdown icon"></i></a>
                                @endauth
                                <a class="item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}</a>
                            </div>
                        </div>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @endguest                    
                </div>
            </div>
        <main class="ui container" style="padding-bottom:50px; min-height:90vh;">
            @include('layouts.messages')
            @yield('content')
        </main>
        <div class="ui inverted vertical footer segment">
            <div class="ui container">
                <div class="ui three column grid">
                    <p class="column">My Sacco App | Etana Tech Limited</p>
                    <p class="column"></p>
                    <p class="column">&copy All Rights Reserved</p>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {  
            $('.ui.checkbox').checkbox();
            $('.ui.dropdown').dropdown({on: 'hover'});
            $('#sacco-main-nav-side-menu').sidebar('attach events', '.sacco-main-nav-side-menu');
            $('ul.pagination').addClass('ui').removeClass('pagination').addClass('pagination menu');
            $('ul.pagination li.page-item').addClass('item').removeClass('page-item');
        });
    </script>
</body>
</html>
