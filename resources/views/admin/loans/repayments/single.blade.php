@extends('layouts.app')

@section('content')
<div class="ui segment">    
    @if($repayment)
        <div class="">
            <h1>Loan Num: {{$repayment->loan->loan_request_id}}<a href="{{route('admin.loans.repayments.index')}}" class="ui tiny button blue right floated inverted"><i class="icon arrow left"></i> Back To Repayments</a></h1>
    
            <div class="ui divider"></div> 
            <p>Payment Reference: {{$repayment->reference}} </p>
            <p>Amount: {{$repayment->amount}} </p>
            <p>Date: {{$repayment->created_at}}</p>
            <p>Status: {{$repayment->statusText}}</p>
        </div>
    @else
        <p>There seems to be something wrong</p>
    @endif
</div>
@endsection