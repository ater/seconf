@extends('layouts.app')

@section('content')
    <div class="ui segment">
        @if(count($loans)>0 && $repayment)
            <h1>Edit Loan repayment<a href="{{route('admin.loans.repayments.index')}}" class="ui tiny button blue right floated inverted"><i class="icon arrow left"></i> Back To Repayments</a></h1>    
            <div class="ui divider"></div> 
            <form class="ui form" method="post" action="{{route('admin.loans.repayments.update',$repayment->reference)}}">
                {{ csrf_field() }}
                <div class="field">
                    <label>Reference Number: {{$repayment->reference}}</label>
                    <input type="hidden" id="" name="reference" class="form-control" placeholder="MPESA reference number" value="{{$repayment->reference}}" required="">
                </div>
                <div class="field">
                    <label>Amount</label>
                    <input type="number" id="" name="amount" class="form-control" placeholder="Amount in KShs" value="{{$repayment->amount}}" required>
                </div>
                <div class="field">
                    <label>Loan Reference</label>
                    <select name="loan_reference" id="sacco-loan-reference" class="ui search dropdown">
                        
                            @foreach($loans as $loan)
                                <option value="{{$loan->loan_request_id}}">{{$loan->loan_request_id}}</option>
                            @endforeach
                    </select>
                </div>
                <div class="field">
                    <label>Repayment Status</label>
                    <select id="repayment-status" name="status" class="ui search dropdown">
                        @if($statuses)
                            @foreach($statuses as $key=>$status)
                                <option value="{{$key}}">{{$status}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                {{ method_field('PUT') }}
                <input type="submit" value="Submit" class="ui button blue"/>
            </form>      
            
            <script>     
                $(document).ready(function() {   
                    $('select.dropdown').dropdown();  
                    $('select#sacco-loan-reference').dropdown('set selected', {{$repayment->loan->loan_request_id}});
                    $('select#repayment-status').dropdown('set selected', {{$repayment->status}});          
                });
            </script>  
        @else
            <h1>Warning</h1>
            You cannot add a new Repayment if no loans active loans are found. Kindly view your <a class="item" href="{{ route('admin.loans.index') }}">{{ __('Loans Here') }}</a>   .
        @endif

    </div>
@endsection