@extends('layouts.app')

@section('content')
<div class="ui">
    <h1>Member Loan Repayments <a href="{{route('admin.loans.repayments.create')}}" class="ui tiny button blue right floated"><i class="icon add"></i> Add Loan Repayment</a></h1>
    
    <div class="ui hidden divider"></div> 

    @if(count($repayments)>0)
        <div class="ui stackable segment grid">
            <div class="four wide sa-white column">
                <div class="sa-grid sa-grid-tile">
                    <h1 class="sa-grid-tile-number no-margin"> {{$repayments->total()}}</h1>
                    <h3 class="sa-grid-tile-title no-margin">Repayments</h3>
                    <small class="sa-grid-tile-description no-margin"></small>
                </div>
            </div>
            @foreach($counts as $key=>$count)
                @if($count!=0)
                    <div class="four wide sa-{{strtolower($key)}} column">
                        <div class="sa-grid sa-grid-tile">
                            <h1 class="sa-grid-tile-number no-margin">{{$count}}</h1>
                            <h3 class="sa-grid-tile-title no-margin">{{$key}}</h3>
                            <small class="sa-grid-tile-description no-margin">
                                <a class="ui button inverted" href="{{route('admin.loans.repayments.index')}}">
                                    <i class="eye icon"></i> View All
                                </a>
                            </small>
                        </div>
                    </div>                
                @endif
            @endforeach
        </div>
        <div class="ui hidden divider"></div>
        <div class="ui hidden divider"></div>
        <div class="ui container">        
            <table class="ui table">
                <thead>
                    <th>Loan</th>
                    <th>Reference Number</th>
                    <th>Amount</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th></th>
                </thead>
                @foreach($repayments as $repayment)
                    <tr>
                        <td><a href="{{route('admin.loans.show',$repayment->loan->loan_request_id)}}">{{$repayment->loan->loan_request_id}}</a></td>
                        <td><a href="{{route('admin.loans.repayments.show',$repayment->reference)}}">{{$repayment->reference}}</td>
                        <td>{{$repayment->amount}}</td>
                        <td>{{$repayment->created_at}}</td>
                        <td>{{$repayment->statusText}}</td>
                        <td><a class="ui tiny button blue" href="{{route('admin.loans.repayments.edit',$repayment->reference)}}">Edit</a></td>
                    </tr>
                @endforeach
            </table>
            {{$repayments->links()}}
        </div>
    @else
        <p>There were no Loan Repayments. So, here is a general Message</p>
    @endif
</div>
@endsection
