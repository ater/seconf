@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Member Loans<a href="{{route('admin.loans.urequests.index')}}" class="tiny ui button blue right floated inverted"> <i class="icon arrow left"></i>Back To Requests</a></h1>
    <div class="ui divider"></div> 
    
    @if(count($loans)>0)
        <div class="ui stackable segment grid">
            <div class="four wide sa-white column">
                <div class="sa-grid sa-grid-tile">
                    <h1 class="sa-grid-tile-number no-margin"> {{$loans->total()}}</h1>
                    <h3 class="sa-grid-tile-title no-margin">Loans</h3>
                    <small class="sa-grid-tile-description no-margin"></small>
                </div>
            </div>
            @foreach($counts as $key=>$count)
                @if($count!=0)
                    <div class="four wide sa-{{strtolower($key)}} column">
                        <div class="sa-grid sa-grid-tile">
                            <h1 class="sa-grid-tile-number no-margin">{{$count}}</h1>
                            <h3 class="sa-grid-tile-title no-margin">{{$key}}</h3>
                            <small class="sa-grid-tile-description no-margin">
                                <a class="ui button inverted" href="{{route('admin.loans.index')}}">
                                    <i class="eye icon"></i> View All
                                </a>
                            </small>
                        </div>
                    </div>                
                @endif
            @endforeach
        </div>
        <div class="ui hidden divider"></div>
        <div class="ui hidden divider"></div>
        <div class="ui container">
            <table class="ui table">
                <thead>
                    <th>Member</th>
                    <th>Reference Number</th>
                    <th>Amount</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th></th>
                </thead>
                @foreach($loans as $loan)
                    <tr>
                        <td><a href="{{route('admin.members.show',$loan->loan_request->member_profile_id)}}">{{$loan->loan_request->member_profile->fullname}}</a></td>
                        <td><a href="{{route('admin.loans.show',$loan->loan_request_id)}}">{{$loan->loan_request_id}}</a></td>
                        <td>{{$loan->amount}}</td>
                        <td>{{$loan->created_at}}</td>
                        <td>{{$loan->statusText}}</td>
                        <td>
                            @if($loan->status==1)
                                <form class="ui form" method="post" action="{{route('admin.loans.updateStatus',$loan->loan_request_id)}}">
                                    {{ csrf_field() }}
                                    <input type="hidden" value="{{$loan->loan_request_id}}" name="referenced">
                                    <input type="hidden" value="2" name="status">
                                    <input type="submit" value="Activate" class="ui button blue"/>
                                    {{ method_field('PUT') }}
                                </form>
                            @else
                                <a class="ui tiny button blue" href="{{route('admin.loans.show',$loan->loan_request_id)}}">View Details</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </table>
            {{$loans->links()}}
        </div>
    @else
        <p>There were no Contributions. So, here is a general Message</p>
    @endif
</div>
@endsection
