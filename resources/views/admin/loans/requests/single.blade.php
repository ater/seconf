@extends('layouts.app')

@section('content')
<div class="ui segment">    
    @if($loan_request)
        <div class="">
            <h1>Loan Request: {{$loan_request->id}}<a href="{{route('admin.loans.urequests.index')}}" class="tiny ui button blue right floated inverted"> <i class="icon arrow left"></i>Back To Requests</a></h1>
            <div class="ui divider"></div> 
            <p>Amount: {{$loan_request->amount}} </p>
            <p>Reason: {{$loan_request->reason}} </p>
            <p>Loan Type: {{$loan_request->loan_type->title}} </p>
            <p>Date: {{$loan_request->created_at}}</p>
            <p>Requested by: <a href="{{route('admin.members.show',$loan_request->member_profile->id)}}">{{$loan_request->member_profile->fullname}}</a></p>
            <p>Status: {{$loan_request->statusText}}</p>
            @if($loan_request->editable)
                <p><a href="{{route('admin.loans.urequests.edit',$loan_request->id)}}" class="ui tiny button blue">Edit Request</a></p>
            @endif
            @if(($loan_request->loan)!=null)
                <p><a href="{{route('admin.loans.show',$loan_request->id)}}" class="ui tiny button blue">View Loan <i class="icon arrow right"></i></a></p>
            @endif
        </div>
    @else
        <p>There seems to be something wrong</p>
    @endif
</div>
@endsection