@extends('layouts.app')

@section('content')
    <div class="ui segment">
        <h1>New Loan Request <a href="{{route('admin.loans.urequests.index')}}" class="tiny ui button blue right floated inverted"> <i class="icon arrow left"></i>Back To Requests</a></h1>
        <div class="ui divider"></div> 
        <form class="ui form" method="post" action="{{ route('admin.loans.urequests.store') }}">
            {{ csrf_field() }}
            <div class="field">
                <label>Amount</label>
                <input type="number" id="" name="amount" class="form-control" placeholder="Amount in KShs" required="" autofocus="">
            </div>
            <div class="field">
                <label>Requested Duration(in Months)</label>
                <input type="number" id="" name="duration" class="form-control" placeholder="Period in Months" required="">
            </div>
            <div class="field">
                <label>Kindly provide a Brief Reason for the loan request</label>
                <textarea name="reason" rows="2" required></textarea>
            </div>
            <div class="field">
                <label>Loan Type</label>
                <select name="loan_type" class="ui search dropdown" required>
                    @if($loan_types)
                        @foreach($loan_types as $loan_type)
                            <option value="{{$loan_type->id}}">{{$loan_type->title}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="ui grid">
                <div class="field">
                    <label>Member Name</label>
                    <select id="sacco-member-name" class="ui search dropdown">
                            @if($members)
                            @foreach($members as $member)
                                <option value="{{$member->id}}">{{$member->fullname}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="field">
                    <label>Member Id</label>
                    <select id="sacco-member-id" name="member_id" class="ui search dropdown">
                            @if($members)
                            @foreach($members as $member)
                                <option value="{{$member->id}}">{{$member->id}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            
            <div class="field">
                    <div class="ui checkbox">
                        <input type="checkbox" tabindex="0" class="hidden" required>
                        <label>Member agreed to the Terms and Conditions</label>
                    </div>
                </div>
            <div class="field">
                <input type="submit" value="Submit" class="ui button blue"/>
            </div>
        </form>
        <script>     
            $(document).ready(function() {   
                $('select.dropdown').dropdown();
                $('select#sacco-member-id').change(()=>{
                    //Make sure they aren't already similar. Trying to prevent infinite loop
                    if($('select#sacco-member-id').val()!==$('select#sacco-member-name').val()){
                        $('select#sacco-member-name').dropdown('set selected', $('select#sacco-member-id').val() );
                        console.log("Hey");
                    }
                });
                $('select#sacco-member-name').change(()=>{
                    //Make sure they aren't already similar. Trying to prevent infinite loop
                    if($('select#sacco-member-id').val()!==$('select#sacco-member-name').val()){
                        $('select#sacco-member-id').dropdown('set selected', $('select#sacco-member-name').val() );
                        console.log("Hey");
                    }
                });
            });
        </script>
    </div>
@endsection