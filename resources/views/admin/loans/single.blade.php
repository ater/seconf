@extends('layouts.app')

@section('content')
<div class="ui">    
    @if($loan)
        <div class="ui segment">
            <h1>Loan <a href="{{route('admin.loans.index')}}" class="tiny ui button blue right floated inverted"> <i class="icon arrow left"></i>Back To Loans</a></h1>
        </div>
        <div class="ui segment">
            <p>Refernce Num: {{$loan->loan_request_id}}</p>            
            <p>Requested By: {{$loan->loan_request->member_profile->fullname}}</p>
            <p>Amount: {{$loan->amount}} </p>
            <p>Date: {{$loan->created_at}}</p>
            <p>Status: {{$loan->statusText}}</p>
            <p>
                @if($loan->status==1)
                    <form class="ui form" method="post" action="{{route('admin.loans.updateStatus',$loan->loan_request_id)}}">
                        {{ csrf_field() }}
                        <input type="hidden" value="{{$loan->loan_request_id}}" name="referenced">
                        <input type="hidden" value="2" name="status">
                        <input type="submit" value="Activate" class="ui button blue"/>
                        {{ method_field('PUT') }}
                    </form>
                @endif
            </p>
        </div>
        <div class="ui divider"></div> 
        @if($repayments && count($repayments)>0)     
            <div class="ui stackable segment grid">
                <div class="four wide sa-white column">
                    <div class="sa-grid sa-grid-tile">
                        <h1 class="sa-grid-tile-number no-margin"> {{$repayments->total()}}</h1>
                        <h3 class="sa-grid-tile-title no-margin">Loan Repayments</h3>
                        <small class="sa-grid-tile-description no-margin"><a href="{{route('admin.loans.repayments.create')}}" class="ui tiny button blue">Add Repayment</a></small>
                    </div>
                </div>
                @if($counts)
                    @foreach($counts as $key=>$count)
                        @if($count!=0)
                            <div class="four wide sa-{{strtolower($key)}} column">
                                <div class="sa-grid sa-grid-tile">
                                    <h1 class="sa-grid-tile-number no-margin">{{$count}}</h1>
                                    <h3 class="sa-grid-tile-title no-margin">{{$key}}</h3>
                                    <small class="sa-grid-tile-description no-margin">
                                        <a class="ui button inverted" href="{{route('loans.index')}}">
                                            <i class="eye icon"></i> View All
                                        </a>
                                    </small>
                                </div>
                            </div>                
                        @endif
                    @endforeach
                @endif
            </div>
            
            <table class="ui table">
                <thead>
                    <th>Reference Number</th>
                    <th>Amount</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th></th>
                </thead>
                @foreach($repayments as $repayment)
                    <tr>
                        <td><a href="{{route('admin.loans.repayments.show',$repayment->reference)}}">{{$repayment->reference}}</td>
                        <td>{{$repayment->amount}}</td>
                        <td>{{$repayment->created_at}}</td>
                        <td>{{$repayment->statusText}}</td>
                    </tr>
                @endforeach
            </table>
        @endif
    @else
        <p>There seems to be something wrong</p>
    @endif
</div>
@endsection