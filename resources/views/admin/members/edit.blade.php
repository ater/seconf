@extends('layouts.app')

@section('content')
    <div class="ui segment">
        <h3>Edit Profile Details for Account <a href="{{route('admin.members.index')}}" class="ui tiny button blue right floated inverted"><i class="icon arrow left"></i> Back To Members</a></h3>
        <div class="ui divider"></div> 
        <form class="ui form" method="post" action="{{route('admin.members.update',$member->id)}}">
            {{ csrf_field() }}
            <div class="field">
                <label>Name</label>
                <input type="text" id="" name="name" class="form-control" placeholder="Full Name" value="{{$member->fullname}}" required="" autofocus="">
            </div>
            <div class="field">
                <label>Id Number</label>
                <input type="text" id="" name="national" class="form-control" placeholder="Id Num" value="{{$member->national_id}}" required="">
            </div>
            <div class="field">
                <label>Phone</label>
                <input type="text" id="" name="phone" class="form-control" placeholder="Phone" value="{{$member->phone}}" required="">
            </div>
            <div class="field">
                <label>Postal</label>
                <input type="text" id="" name="postal" class="form-control" placeholder="Postal" value="{{$member->postal}}" required="">
            </div>
            <div class="field">
                <label>Account Status</label>
                <select id="sacco-member-status" name="status" class="ui search dropdown">
                    @if($statuses)
                        @foreach($statuses as $key=>$status)
                            <option value="{{$key}}">{{$status}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            
            <input type="hidden" value="{{$member->user->email}}" name="email">
            <input type="hidden" value="{{$member->id}}" name="member_num">
            {{ method_field('PUT') }}
       
            <div class="field">
                <label></label>
                <input type="submit" value="Submit" class="ui button blue"/>
            </div>
        </form>
        
        <script>     
            $(document).ready(function() {   
                $('select#sacco-member-status').dropdown('set selected', {{$member->status}});
            });
        </script>
    </div>
@endsection