@extends('layouts.app')

@section('content')
<div class="ui segment">
    @if($member)
        <h1>Profile {{$member->id}}<a href="{{route('admin.members.edit',$member->id)}}" class="ui tiny button blue right floated">Edit Member Profile</a></h1>
        <div class="ui divider"></div> 
        <p>Hey {{$member->fullname}} </p>
        <p>ID Number: {{$member->national_id}} </p>
        <p>Phone Number: {{$member->phone}} </p>
        <p>Postal: {{$member->postal}} </p>
        <p>Status: {{$member->statusText}} </p>
    @else
        <p>There was an error. Kindly ensure you are a member</p>
    @endif
</div>
@endsection
