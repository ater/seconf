@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Sacco Members<a href="{{route('admin.users.index')}}" class="ui tiny button blue right floated"><i class="icon eye"></i> View Registered Users</a></h1>
    
    <div class="ui hidden divider"></div> 
    @if(count($members)>0)
        <div class="ui stackable segment grid">
            <div class="four wide sa-white column">
                <div class="sa-grid sa-grid-tile">
                    <h1 class="sa-grid-tile-number no-margin"> {{$members->total()}}</h1>
                    <h3 class="sa-grid-tile-title no-margin">Members</h3>
                    <small class="sa-grid-tile-description no-margin"></small>
                </div>
            </div>
            @foreach($counts as $key=>$count)
                @if($count!=0)
                    <div class="four wide sa-{{strtolower($key)}} column">
                        <div class="sa-grid sa-grid-tile">
                            <h1 class="sa-grid-tile-number no-margin">{{$count}}</h1>
                            <h3 class="sa-grid-tile-title no-margin">{{$key}}</h3>
                            <small class="sa-grid-tile-description no-margin">
                                <a class="ui button inverted" href="{{route('admin.members.index')}}">
                                    <i class="eye icon"></i> View All
                                </a>
                            </small>
                        </div>
                    </div>                
                @endif
            @endforeach
        </div>
        <div class="ui hidden divider"></div>
        <div class="ui hidden divider"></div>
        <div class="ui container"> 
            <table class="ui table">
                <thead>
                    <th>Member Num</th>
                    <th>Name</th>
                    <th>National ID</th>
                    <th>Phone</th>
                    <th>Postal Address</th>
                    <th>Registered</th>
                    <th>Status</th>
                    <th></th>
                </thead>
                @foreach($members as $member)
                    <tr>
                        <td><a href="{{route('admin.members.show',$member->id)}}">{{$member->id}}</a></td>
                        <td>{{$member->fullname}}</td>
                        <td>{{$member->national_id}}</td>
                        <td>{{$member->phone}}</td>
                        <td>{{$member->postal}}</td>
                        <td>{{$member->created_at}}</td>
                        <td>{{$member->statusText}}</td>
                        <td><a class="ui tiny button blue" href="{{route('admin.members.edit',$member->id)}}">Edit</a></td>
                    </tr>
                @endforeach
            </table>
            {{$members->links()}}
        </div>
    @else
        <p>There were no Members found on the system.</p>
    @endif
</div>
@endsection
