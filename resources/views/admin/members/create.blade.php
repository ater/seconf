@extends('layouts.app')

@section('content')
    <div class="container">
        <h3>Add Profile Details for {{$user->email}}</h3>
        <form class="ui form" method="post" action="{{route('admin.members.store')}}">
            {{ csrf_field() }}
            <div class="field">
                <label>Name</label>
                <input type="text" id="" name="name" class="form-control" placeholder="Full Name" required="" autofocus="">
            </div>
            <div class="field">
                <label>Id Number</label>
                <input type="text" id="" name="national" class="form-control" placeholder="Id Num" required="">
            </div>
            <div class="field">
                <label>Phone</label>
                <input type="text" id="" name="phone" class="form-control" placeholder="Phone" required="">
            </div>
            <div class="field">
                <label>Postal</label>
                <input type="text" id="" name="postal" class="form-control" placeholder="Postal" required="">
            </div>
            
            <input type="hidden" value="{{$user->email}}" name="email">
            <input type="submit" value="Submit" class="ui button blue"/>
        </form>
    </div>
@endsection