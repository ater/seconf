@extends('layouts.app')

@section('content')
    <div class="ui container">.
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        
        <div class="ui stackable relaxed grid">
            <div class="row">Welcome Back, Admin</div>
            <div class="four wide sa-white column">
                <div class="sa-grid sa-grid-tile">
                    <h1 class="sa-grid-tile-number no-margin">{{$shares["number"]}}</h1>
                    <h3 class="sa-grid-tile-title no-margin">Shares</h3>
                    <small class="sa-grid-tile-description no-margin">Value: KES: {{$shares["value"]}}</small>
                </div>
            </div>
            <div class="four wide blue column">
                <div class="sa-grid sa-grid-tile">
                    <h1 class="sa-grid-tile-number no-margin">{{$contributions }}</h1>
                    <h3 class="sa-grid-tile-title no-margin">Contributions</h3>
                    <small class="sa-grid-tile-description no-margin">
                        <a class="ui button inverted" href="{{route('admin.contributions.index')}}">
                            <i class="eye icon"></i> View All
                        </a>
                    </small>
                </div>
            </div>
            <div class="four wide yellow column">
                <div class="sa-grid sa-grid-tile">
                    <h1 class="sa-grid-tile-number no-margin">{{$requests }}</h1>
                    <h3 class="sa-grid-tile-title no-margin">Loan Requests</h3>
                    <small class="sa-grid-tile-description no-margin">
                        <a class="ui button inverted" href="{{route('admin.loans.urequests.index')}}">
                            <i class="eye icon"></i> View All
                        </a>
                    </small>
                </div>
            </div>
            <div class="four wide green column">
                <div class="sa-grid sa-grid-tile">
                    <h1 class="sa-grid-tile-number no-margin">{{$loans }}</h1>
                    <h3 class="sa-grid-tile-title no-margin">Loans</h3>
                    <small class="sa-grid-tile-description no-margin">
                        <a class="ui button inverted" href="{{route('admin.loans.index')}}">
                            <i class="eye icon"></i> View All
                        </a>
                    </small>
                </div>
            </div>
        </div>    

        @if($contribution_stats!=null && $requests_by_type !=null)
            <div class="ui grid">
                <div class="row">
                    <h1 class="header">Statistics Overview</h1>
                </div>
            </div>
            <div class="ui two column stackable grid">
                <div class="column">
                    <div class="ui raised segment">
                        <a class="ui blue ribbon label">Value Per Day</a> Contributions
                        <canvas id="contributionChart" width="200" height="100"></canvas>
                    </div>
                </div>
                <div class="column">
                    <div class="ui raised segment">
                        <a class="ui yellow ribbon label">Request By Type</a> Loan Requests
                        <canvas id="loanTypeChart" width="200" height="100"></canvas>
                    </div>
                </div>
            </div>
            
            <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
            <script>
                $(document).ready(function() {  
                    var ctx = document.getElementById("contributionChart").getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'line',
                        data: {
                            labels: [@foreach($contribution_stats['dates'] as $date) "{{$date}}", @endforeach],
                            datasets: [{
                                label: 'Value of Contributions',
                                data: [{{implode(",",$contribution_stats['amount'])}}],
                                backgroundColor: ['rgba(33, 133, 208,0.2)'],
                                borderColor:['rgb(33, 133, 208)']
                            }]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });
                    var ctx_ltc = document.getElementById("loanTypeChart").getContext('2d');
                    var myChart = new Chart(ctx_ltc, {
                        type: 'doughnut',
                        data: {
                            labels: [@foreach($requests_by_type['type'] as $lType) "{{$lType}}", @endforeach],
                            datasets: [{
                                data: [{{implode(",",$requests_by_type['number'])}}],
                                backgroundColor: ['rgba(251, 189, 8,0.8)','rgba(242, 113, 28 ,0.8)','rgba(219, 40, 40,0.8)'],
                                borderColor:['rgb(251, 189, 8)','rgb(242, 113, 28)','rgb(219, 40, 40)']
                            }]
                        }
                        
                    });
                });
            </script>
        @endif
    </div>
@endsection
