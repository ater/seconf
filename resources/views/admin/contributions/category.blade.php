@extends('layouts.app')

@section('content')
<div class="ui">
    @if($contributions && count($contributions)>0)
        <div class="ui segment">
            <h1>{{$status}} Contributions  <a href="{{route('admin.contributions.index')}}" class="tiny ui button blue right floated inverted"> <i class="icon arrow left"></i>Back to Contributions</a></h1>
        </div>
        <table class="ui celled table">
            <thead>
                <th>Member</th>
                <th>Reference Number</th>
                <th>Amount</th>
                <th>Date</th>
                <th>Status</th>
            </thead>
            @foreach($contributions as $contribution)
                <tr>
                    <td><a href="{{route('admin.members.show',$contribution->member_profile_id)}}">{{$contribution->member_profile->fullname}}</a></td>
                    <td><a href="{{route('admin.contributions.show',$contribution->reference)}}">{{$contribution->reference}}</td>
                    <td>{{$contribution->amount}}</td>
                    <td>{{$contribution->created_at}}</td>
                    <td>{{$contribution->statusText}}</td>
                </tr>
            @endforeach
        </table>
        {{$contributions->links()}}
    @else
        <p>There were no Contributions. So, here is a general Message</p>
    @endif
</div>
@endsection
