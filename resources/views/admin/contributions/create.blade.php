@extends('layouts.app')

@section('content')
    <div class="ui segment">
        <h1>Add New Contribution<a href="{{route('admin.contributions.index')}}" class="tiny ui button blue right floated inverted"> <i class="icon arrow left"></i>Back To Contributions</a></h1>
        <div class="ui divider"></div>
        <form class="ui form" method="post" action="{{route('admin.contributions.store')}}">
            {{ csrf_field() }}
            <div class="field">
                <label>Reference Number</label>
                <input type="text" id="" name="reference" class="form-control" placeholder="MPESA reference number" required="" autofocus="">
            </div>
            <div class="field">
                <label>Amount</label>
                <input type="number" id="" name="amount" class="form-control" placeholder="Amount in KShs" required="">
            </div>            
            <div class="ui grid">
                <div class="field">
                    <label>Member Name</label>
                    <select id="sacco-member-name" class="ui search dropdown">
                            @if($members)
                            @foreach($members as $member)
                                <option value="{{$member->id}}">{{$member->fullname}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="field">
                    <label>Member Id</label>
                    <select id="sacco-member-id" name="member_id" class="ui search dropdown">
                            @if($members)
                            @foreach($members as $member)
                                <option value="{{$member->id}}">{{$member->id}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="ui hidden divider"></div>
            <input type="submit" value="Submit" class="ui button blue"/>
        </form>
       
    </div>
    <script>     
        $(document).ready(function() {   
            $('select.dropdown').dropdown();
            $('select#sacco-member-id').change(()=>{
                //Make sure they aren't already similar. Trying to prevent infinite loop
                if($('select#sacco-member-id').val()!==$('select#sacco-member-name').val()){
                    $('select#sacco-member-name').dropdown('set selected', $('select#sacco-member-id').val() );
                    console.log("Hey");
                }
            });
            $('select#sacco-member-name').change(()=>{
                //Make sure they aren't already similar. Trying to prevent infinite loop
                if($('select#sacco-member-id').val()!==$('select#sacco-member-name').val()){
                    $('select#sacco-member-id').dropdown('set selected', $('select#sacco-member-name').val() );
                    console.log("Hey");
                }
            });
        });
    </script>
@endsection