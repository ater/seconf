@extends('layouts.app')

@section('content')
<div class="ui segment">    
    @if($contribution)
        <div class="well">
            <h1 class="ui header">Contribution <a href="{{route('admin.contributions.index')}}" class="tiny ui button blue right floated inverted"> <i class="icon arrow left"></i>Back To Contributions</a></h1>
            <div class="ui divider"></div> 
            <p>Reference Number: {{$contribution->reference}}</p>
            <p>Amount: {{$contribution->amount}} </p>
            <p>Date: {{$contribution->created_at}}</p>
            <p>Member: {{$contribution->member_profile->fullname}}</p>
            <p>Status: {{$contribution->StatusText}}</p>
            <p><a class="ui tiny button blue" href="{{route('admin.contributions.edit',$contribution->reference)}}">Edit</a></p>
        </div>
    @else
        <p>There seems to be something wrong</p>
    @endif
</div>
@endsection
