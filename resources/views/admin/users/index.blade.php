@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Platform Users</h1>
    
    @if(count($users)>0)

        <h1>{{$counted}} Registered Users</h1>
        <table class="ui table">
            <thead>
                <th>User Name</th>
                <th>Email</th>
                <th>Date</th>
                <th>Profile</th>
            </thead>
            @foreach($users as $user)
                <tr>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->created_at}}</td>
                    <td>
                        @if($user->profile)
                            There seems to be a profile
                        @else
                            <a href="users/createProfile/{{$user->email}}">Create a profile for this user</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>
        {{$users->links()}}
    @else
        <p>There were no Users Found.</p>
    @endif
</div>
@endsection
