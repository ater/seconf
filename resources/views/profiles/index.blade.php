@extends('layouts.app')

@section('content')
    <div class="ui segment">
        <h1>My Profile <a href="/profile/edit" class="ui tiny button blue right floated">Edit Profile</a></h1>
    </div>
    <div class="ui divider"></div> 
    <div class="ui segment">
        @if(Auth::user()->profile)
            <p>Hey {{Auth::user()->profile->fullname}} </p>
            <p>ID Number: {{$profile->national_id}} </p>
            <p>Phone Number: {{$profile->phone}} </p>
            <p>Postal: {{$profile->postal}} </p>
            <p>Status: {{$profile->statusText}} </p>
        @else
            <p>There was an error. Kindly ensure you are a member</p>
        @endif
    </div>
@endsection
