@extends('layouts.app')

@section('content')
    <div class="ui segment">
        <h3>Edit Your Profile Details</h3>
        <div class="ui divider"></div> 
        <form class="ui form" method="post" action="/profile">
            {{ csrf_field() }}
            <div class="field">
                <label>Name</label>
                <input type="text" id="" name="name" class="form-control" placeholder="Full Name" value="{{Auth::user()->profile->fullname}}" required="" autofocus="">
            </div>
            <div class="field">
                <label>Id Number</label>
                <input type="text" id="" name="national" class="form-control" placeholder="Id Num" value="{{$profile->national_id}}" required="">
            </div>
            <div class="field">
                <label>Phone</label>
                <input type="text" id="" name="phone" class="form-control" placeholder="Phone" value="{{$profile->phone}}" required="">
            </div>
            <div class="field">
                <label>Postal</label>
                <input type="text" id="" name="postal" class="form-control" placeholder="Postal" value="{{$profile->postal}}" required="">
            </div>
            
            <input type="hidden" value="{{Auth::user()->email}}" name="email">
            <input type="hidden" value="{{$profile->id}}" name="member_num">
            {{ method_field('PUT') }}
       
            <div class="field">
                <label></label>
                <input type="submit" value="Submit" class="ui button blue"/>
            </div>
        </form>
    </div>
@endsection