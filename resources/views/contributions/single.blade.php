@extends('layouts.app')

@section('content')
<div class="ui segment">    
    @if($contribution)
        <div class="well">
            <h1 class="ui header">My Contributions <a href="{{route('contributions.create')}}" class="tiny ui button blue right floated"> <i class="icon add"></i>Add Contribution</a></h1>
            <div class="ui divider"></div> 
            <p>Reference Number: {{$contribution->reference}}</p>
            <p>Amount: {{$contribution->amount}} </p>
            <p>Date: {{$contribution->created_at}}</p>
            <p>Status: {{$contribution->StatusText}}</p>
        </div>
    @else
        <p>There seems to be something wrong</p>
    @endif
</div>
@endsection
