@extends('layouts.app')

@section('content')
<div class="ui containter">
    <h1 class="ui header">My Contributions <a href="{{route('contributions.create')}}" class="tiny ui button blue right floated"> <i class="icon add"></i>Add Contribution</a></h1>
    <div class="ui hidden divider"></div>
    @if(count($contributions)>0)
        <div class="ui stackable segment grid">
            <div class="four wide sa-white column">
                <div class="sa-grid sa-grid-tile">
                    <h1 class="sa-grid-tile-number no-margin"> {{$contributions->total()}}</h1>
                    <h3 class="sa-grid-tile-title no-margin">Contributions</h3>
                    <small class="sa-grid-tile-description no-margin">Approved Value, KES: {{$shares["value"]}}</small>
                </div>
            </div>
            @foreach($counts as $key=>$count)
                @if($count!=0)
                    <div class="four wide sa-{{strtolower($key)}} column">
                        <div class="sa-grid sa-grid-tile">
                            <h1 class="sa-grid-tile-number no-margin">{{$count}}</h1>
                            <h3 class="sa-grid-tile-title no-margin">{{$key}}</h3>
                            <small class="sa-grid-tile-description no-margin">
                                <a class="ui button inverted" href="{{route('contributions.category.index',$key)}}">
                                    <i class="eye icon"></i> View All
                                </a>
                            </small>
                        </div>
                    </div>                
                @endif
            @endforeach
        </div>
        <div class="ui hidden divider"></div>
        <div class="ui hidden divider"></div>
        <div class="ui container">
            <table class="ui celled table">
                <thead>
                    <th>Reference Number</th>
                    <th>Amount</th>
                    <th>Date</th>
                    <th>Status</th>
                </thead>
                @foreach($contributions as $contribution)
                    <tr>
                        <td><a href="/contributions/{{$contribution->reference}}">{{$contribution->reference}}</td>
                        <td>{{$contribution->amount}}</td>
                        <td>{{$contribution->created_at}}</td>
                        <td><span class="sa-{{strtolower($contribution->statusText)}}">_</span> {{$contribution->statusText}} </td>
                    </tr>
                @endforeach
            </table>
            {{$contributions->links()}}
        </div>
    @else
        <p>There were no Contributions. So, here is a general Message</p>
    @endif
</div>
@endsection
