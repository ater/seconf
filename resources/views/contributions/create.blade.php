@extends('layouts.app')

@section('content')
    <div class="ui segment">
        <h1>Add New Contribution <a href="{{route('contributions.index')}}" class="tiny ui button blue right floated inverted"> <i class="icon arrow left"></i>Back to Contributions</a></h1>
        <div class="ui divider"></div> 
        <form class="ui form" method="post" action="{{route('contributions.store')}}">
            {{ csrf_field() }}
            <div class="field">
                <label>Mpesa Reference Number</label>
                <input type="text" id="" name="reference" class="form-control" placeholder="MPESA reference number" required="" autofocus="">
            </div>
            <div class="field">
                <label>Amount</label>
                <input type="number" id="" name="amount" class="form-control" placeholder="Amount in KShs" required="" autofocus="">
            </div>
            <div class="field">
                <input type="submit" value="Submit" class="ui button blue"/>
            </div>
        </form>
    </div>
@endsection