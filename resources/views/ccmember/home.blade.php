@extends('layouts.app')

@section('content')
    <div class="ui container">.
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="ui grid">
            Hey CC Member {{Auth::user()->name}}
        </div>
    </div>

@endsection

