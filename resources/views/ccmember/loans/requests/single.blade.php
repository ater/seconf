@extends('layouts.app')

@section('content')
<div class="ui">    
    @if($loan_request)
        <div class="ui segment">
            <h1>Loan Request: {{$loan_request->id}} <a href="{{route('ccmember.loans.urequests.index')}}" class="tiny ui button blue right floated inverted"> <i class="icon arrow left"></i>Back to Requests</a></h1>
            <div class="ui divider"></div> 
            <p>Amount: {{$loan_request->amount}} </p>
            <p>Reason: {{$loan_request->reason}} </p>
            <p>Loan Type: {{$loan_request->loan_type->title}} </p>
            <p>Date: {{$loan_request->created_at}}</p>
            <p>Status: {{$loan_request->statusText}}</p>
            @if(($loan_request->loan)!=null)
                <p><a href="{{route('loans.show',$loan_request->id)}}" class="ui tiny button blue">View Loan <i class="icon arrow right"></i></a></p>
            @endif
            <div class="ui stackable grid">
                @if($loan_request->status==12)
                    @if($ccmember_has_given_feedback) 
                        <form class="ui two wide column" method="post" action="{{route('ccmember.loans.urequests.feedback.update',[$loan_request->id,$ccmember_has_given_feedback->id])}}">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{$loan_request->id}}" name="loan_ref">
                            @if($ccmember_has_given_feedback->status==2)                        
                                <input type="hidden" value="3" name="status">
                                <input type="submit" value="Reject" class="ui red button"/>
                            @else
                                <input type="hidden" value="2" name="status">
                                <input type="submit" value="Approve" class="ui button blue"/>
                            @endif                
                            {{ method_field('PUT') }}
                        </form>
                    @else
                        <form class="ui two wide column" method="post" action="{{route('ccmember.loans.urequests.feedback.store',$loan_request->id)}}">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{$loan_request->id}}" name="loan_ref">
                            <input type="hidden" value="3" name="status">
                            <input type="submit" value="Reject" class="ui button red"/>              
                        </form>
                        <form class="ui two wide column" method="post" action="{{route('ccmember.loans.urequests.feedback.store',$loan_request->id)}}">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{$loan_request->id}}" name="loan_ref">
                            <input type="hidden" value="2" name="status">
                            <input type="submit" value="Approve" class="ui button blue"/>              
                        </form>
                    @endif
                @endif
            </div>
        </div>
        
        @if(count($cc_feedback)>0)
            <div class="ui hidden divider"></div> 
            <div class="ui segment">
                <h1>Loan Request Feedback 
                    <form class="ui two wide column" method="post" action="{{route('ccmember.loans.urequests.update',$loan_request->id)}}">
                        {{ csrf_field() }}
                        <input type="hidden" value="{{$loan_request->id}}" name="id">
                        <input type="hidden" value="2" name="status">
                        <input type="submit" value="Confrim Committee Approval" class="ui button blue"/> 
                        {{ method_field('PUT') }}            
                    </form>
                </h1>
                <table class="ui table">
                    <thead>
                        <th>Name</th>
                        <th>Date</th>
                        <th>Status</th>
                    </thead>
                    @foreach($cc_feedback as $single_feedback)
                        <tr>
                            <td>{{$single_feedback->ccmember->name}}</td>
                            <td>{{$single_feedback->created_at}}</td>
                            <td><span class="sa-{{strtolower($single_feedback->statusText)}}">_</span> {{$single_feedback->statusText}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="ui hidden divider"></div> 
        @endif
        @if($loan_request->guarantorRequired)
            <div class="ui hidden divider"></div> 
            <div class="ui segment">
                <h1>Loan Guarantors </h1>
            </div>
            <div class="ui hidden divider"></div> 
        @endif
        @if(count($guarantors)>0)     
            <div class="ui stackable segment grid">
                <div class="four wide sa-white column">
                    <div class="sa-grid sa-grid-tile">
                        <h1 class="sa-grid-tile-number no-margin"> {{$guarantors->total()}}</h1>
                        <h3 class="sa-grid-tile-title no-margin">Loan Guarantors</h3>
                        <small class="sa-grid-tile-description no-margin">{{$total_guaranteed}} of {{$loan_request->amount}} Guaranteed</small>
                    </div>
                </div>
                @if($counts)
                    @foreach($counts as $key=>$count)
                        @if($count!=0)
                            <div class="four wide sa-{{strtolower($key)}} column">
                                <div class="sa-grid sa-grid-tile">
                                    <h1 class="sa-grid-tile-number no-margin">{{$count}}</h1>
                                    <h3 class="sa-grid-tile-title no-margin">{{$key}}</h3>
                                    <small class="sa-grid-tile-description no-margin">
                                        <a class="ui button inverted" href="{{route('loans.urequests.show',$loan_request->id)}}">
                                            <i class="eye icon"></i> View All
                                        </a>
                                    </small>
                                </div>
                            </div>                
                        @endif
                    @endforeach
                @endif
            </div>
            
            <table class="ui table">
                <thead>
                    <th>Name</th>
                    <th>Member Num</th>
                    <th>Amount(in Kshs.)</th>
                    <th>Date</th>
                    <th>Status</th>
                </thead>
                @foreach($guarantors as $guarantor)
                    <tr>
                        <td>{{$guarantor->member_profile->fullname}}</td>
                        <td>{{$guarantor->member_profile_id}}</td>
                        <td>{{$guarantor->amount}}</td>
                        <td>{{$guarantor->created_at}}</td>
                        <td><span class="sa-{{strtolower($guarantor->statusText)}}">_</span> {{$guarantor->statusText}}</td>
                    </tr>
                @endforeach
            </table>
        @endif
    @else
        <p>There seems to be something wrong</p>
    @endif
</div>
@endsection