@extends('layouts.app')

@section('content')
<div class="ui container">
    <h1>Loan Requests </h1>
    <div class="ui divider hidden"></div> 
    @if(count($loan_requests)>0)
        <div class="ui stackable segment grid">
            <div class="four wide sa-white column">
                <div class="sa-grid sa-grid-tile">
                    <h1 class="sa-grid-tile-number no-margin"> {{$loan_requests->total()}}</h1>
                    <h3 class="sa-grid-tile-title no-margin">Loan Requests</h3>
                    <small class="sa-grid-tile-description no-margin"></small>
                </div>
            </div>
            @if($counts)
                @foreach($counts as $key=>$count)
                    @if($count!=0)
                        <div class="four wide sa-{{strtolower($key)}} column">
                            <div class="sa-grid sa-grid-tile">
                                <h1 class="sa-grid-tile-number no-margin">{{$count}}</h1>
                                <h3 class="sa-grid-tile-title no-margin">{{$key}}</h3>
                                <small class="sa-grid-tile-description no-margin">
                                    <a class="ui button inverted" href="{{route('ccmember.loans.urequests.index')}}">
                                        <i class="eye icon"></i> View All
                                    </a>
                                </small>
                            </div>
                        </div>                
                    @endif
                @endforeach
            @endif
        </div>
        <div class="ui hidden divider"></div>
        <div class="ui hidden divider"></div>
        <div class="ui container">
            <table class="ui celled table">
                <thead>
                    <th>Reference Number</th>
                    <th>Amount(in Kshs.)</th>
                    <th>Duration (in Months)</th>
                    <th>Reason</th>
                    <th>Loan Type</th>
                    <th>Date</th>
                    <th>Status</th>
                </thead>
                @foreach($loan_requests as $loan_request)
                    <tr>
                        <td><a href="{{route('ccmember.loans.urequests.show',$loan_request->id)}}">{{$loan_request->id}}</td>
                        <td>{{$loan_request->amount}}</td>
                        <td>{{$loan_request->duration}}</td>
                        <td>{{$loan_request->reason}}</td>
                        <td>{{$loan_request->loan_type->title}}</td>
                        <td>{{$loan_request->created_at}}</td>
                        <td><span class="sa-{{strtolower($loan_request->statusText)}}">_</span> {{$loan_request->statusText}}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    @else
        <p>There were no loan requests. So, here is a general Message</p>
    @endif
</div>
@endsection