@extends('layouts.app')

@section('content')
<div class="container">.
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        
        <div class="ui stackable relaxed grid">
            <div class="row">Welcome Back</div>
            <div class="four wide sa-white column">
                <div class="sa-grid sa-grid-tile">
                    <h1 class="sa-grid-tile-number no-margin">{{$shares["number"]}}</h1>
                    <h3 class="sa-grid-tile-title no-margin">Shares</h3>
                    <small class="sa-grid-tile-description no-margin">Value: KES: {{$shares["value"]}}</small>
                </div>
            </div>
            <div class="four wide blue column">
                <div class="sa-grid sa-grid-tile">
                    <h1 class="sa-grid-tile-number no-margin">{{$contributions }}</h1>
                    <h3 class="sa-grid-tile-title no-margin">Contributions</h3>
                    <small class="sa-grid-tile-description no-margin">
                        <a class="ui button inverted" href="{{route('contributions.index')}}">
                            <i class="eye icon"></i> View All
                        </a>
                    </small>
                </div>
            </div>
            <div class="four wide yellow column">
                <div class="sa-grid sa-grid-tile">
                    <h1 class="sa-grid-tile-number no-margin">{{$requests }}</h1>
                    <h3 class="sa-grid-tile-title no-margin">Loan Requests</h3>
                    <small class="sa-grid-tile-description no-margin">
                        <a class="ui button inverted" href="{{route('loans.urequests.index')}}">
                            <i class="eye icon"></i> View All
                        </a>
                    </small>
                </div>
            </div>
            <div class="four wide green column">
                <div class="sa-grid sa-grid-tile">
                    <h1 class="sa-grid-tile-number no-margin">{{$loans }}</h1>
                    <h3 class="sa-grid-tile-title no-margin">Loans</h3>
                    <small class="sa-grid-tile-description no-margin">
                        <a class="ui button inverted" href="{{route('loans.index')}}">
                            <i class="eye icon"></i> View All
                        </a>
                    </small>
                </div>
            </div>
        </div>
          
</div>
@endsection
