@extends('layouts.app')

@section('content')
<div class="ui">
    <div class="ui segment">
        <h1>Loan {{$loan_ref}} Repayments <a href="{{route('loans.repayments.create',$loan_ref)}}" class="ui tiny button blue right floated">Add Loan Repayment</a></h1>
    </div>
    <div class="ui hidden divider"></div> 
    @if(count($repayments)>0)
        <div class="ui stackable segment grid">
            <div class="four wide sa-white column">
                <div class="sa-grid sa-grid-tile">
                    <h1 class="sa-grid-tile-number no-margin"> {{$repayments->total()}}</h1>
                    <h3 class="sa-grid-tile-title no-margin">Loan repayments</h3>
                    <small class="sa-grid-tile-description no-margin"></small>
                </div>
            </div>
            @foreach($counts as $key=>$count)
                @if($count!=0)
                    <div class="four wide sa-{{strtolower($key)}} column">
                        <div class="sa-grid sa-grid-tile">
                            <h1 class="sa-grid-tile-number no-margin">{{$count}}</h1>
                            <h3 class="sa-grid-tile-title no-margin">{{$key}}</h3>
                            <small class="sa-grid-tile-description no-margin">
                                <a class="ui button inverted" href="{{route('loans.index')}}">
                                    <i class="eye icon"></i> View All
                                </a>
                            </small>
                        </div>
                    </div>                
                @endif
            @endforeach
        </div>
        <div class="ui hidden divider"></div>
        <div class="ui hidden divider"></div>
        <div class="ui container">
            <table class="ui table">
                <thead>
                    <th>Loan</th>
                    <th>Reference Number</th>
                    <th>Amount</th>
                    <th>Date</th>
                    <th>Status</th>
                </thead>
                @foreach($repayments as $repayment)
                    <tr>
                        <td><a href="{{route('loans.show',$loan_ref)}}">{{$loan_ref}}</a></td>
                        <td><a href="{{route('loans.repayments.show',[$loan_ref,$repayment->reference])}}">{{$repayment->reference}}</td>
                        <td>{{$repayment->amount}}</td>
                        <td>{{$repayment->created_at}}</td>
                        <td>{{$repayment->statusText}}</td>
                    </tr>
                @endforeach
            </table>
        </div>
        
    @else
        <p>There were no Loan Repayments. So, here is a general Message</p>
    @endif
</div>
@endsection
