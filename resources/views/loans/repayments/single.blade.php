@extends('layouts.app')

@section('content')
<div class="ui segment">    
    @if($repayment)
        <div class="">
            <h1>Repayment for Loan Num: {{$loan_ref}}<a href="{{route('loans.repayments.create',$loan_ref)}}" class="tiny ui button blue right floated"> <i class="icon add"></i>Add New Repayment</a></h1>
            <div class="ui divider"></div> 
            <p>Payment Reference: {{$repayment->reference}} </p>
            <p>Amount: {{$repayment->amount}} </p>
            <p>Date: {{$repayment->created_at}}</p>
            <p>Status: {{$repayment->statusText}}</p>
            <p><a href="{{route('loans.show',$loan_ref)}}" class="tiny ui button blue"> <i class="icon eye"></i>View Loan</a></p>
        </div>
    @else
        <p>There seems to be something wrong</p>
    @endif
</div>
@endsection