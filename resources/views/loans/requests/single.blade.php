@extends('layouts.app')

@section('content')
<div class="ui">    
    @if($loan_request)
        <div class="ui segment">
            <h1>Loan Request: {{$loan_request->id}} <a href="{{route('loans.urequests.index')}}" class="tiny ui button blue right floated inverted"> <i class="icon arrow left"></i>Back to Requests</a></h1>
            <div class="ui divider"></div> 
            <p>Amount: {{$loan_request->amount}} </p>
            <p>Reason: {{$loan_request->reason}} </p>
            <p>Loan Type: {{$loan_request->loan_type->title}} </p>
            <p>Date: {{$loan_request->created_at}}</p>
            <p>Status: {{$loan_request->statusText}}</p>
            @if(($loan_request->loan)!=null)
                <p><a href="{{route('loans.show',$loan_request->id)}}" class="ui tiny button blue">View Loan <i class="icon arrow right"></i></a></p>
            @endif
        </div>
        @if($loan_request->guarantorRequired)
            <div class="ui hidden divider"></div> 
            <div class="ui segment">
                <h1>Loan Guarantors 
                @if($loan_request->status==10 || $loan_request->status==11)
                        @if($total_guaranteed>=$loan_request->amount)
                            <form class="ui form" method="post" action="{{route('loans.urequests.sendToCommittee',$loan_request->id)}}">
                                {{ csrf_field() }}
                                <input type="hidden" value="12" name="status">
                                <input type="submit" value="Request Review" class="ui button tiny green"/>
                                {{ method_field('PUT') }}
                            </form>                        
                        @else
                            <a href="{{route('loans.guarantors.create',$loan_request->id)}}" class="ui tiny button blue right floated"><i class="icon add"></i> Add Guarantor</a>                        
                        @endif
                    @endif
                </h1>
            </div>
            <div class="ui hidden divider"></div> 
        @endif
        @if(count($guarantors)>0)     
            <div class="ui stackable segment grid">
                <div class="four wide sa-white column">
                    <div class="sa-grid sa-grid-tile">
                        <h1 class="sa-grid-tile-number no-margin"> {{$guarantors->total()}}</h1>
                        <h3 class="sa-grid-tile-title no-margin">Loan Guarantors</h3>
                        <small class="sa-grid-tile-description no-margin">{{$total_guaranteed}} of {{$loan_request->amount}} Guaranteed</small>
                    </div>
                </div>
                @if($counts)
                    @foreach($counts as $key=>$count)
                        @if($count!=0)
                            <div class="four wide sa-{{strtolower($key)}} column">
                                <div class="sa-grid sa-grid-tile">
                                    <h1 class="sa-grid-tile-number no-margin">{{$count}}</h1>
                                    <h3 class="sa-grid-tile-title no-margin">{{$key}}</h3>
                                    <small class="sa-grid-tile-description no-margin">
                                        <a class="ui button inverted" href="{{route('loans.urequests.show',$loan_request->id)}}">
                                            <i class="eye icon"></i> View All
                                        </a>
                                    </small>
                                </div>
                            </div>                
                        @endif
                    @endforeach
                @endif
            </div>
            
            <table class="ui table">
                <thead>
                    <th>Name</th>
                    <th>Member Num</th>
                    <th>Amount(in Kshs.)</th>
                    <th>Date</th>
                    <th>Status</th>
                </thead>
                @foreach($guarantors as $guarantor)
                    <tr>
                        <td>{{$guarantor->member_profile->fullname}}</td>
                        <td>{{$guarantor->member_profile_id}}</td>
                        <td>{{$guarantor->amount}}</td>
                        <td>{{$guarantor->created_at}}</td>
                        <td><span class="sa-{{strtolower($guarantor->statusText)}}">_</span> {{$guarantor->statusText}}</td>
                    </tr>
                @endforeach
            </table>
        @endif
    @else
        <p>There seems to be something wrong</p>
    @endif
</div>
@endsection