@extends('layouts.app')

@section('content')
    <div class="ui segment">
        <h1>Request Loan<a href="{{route('loans.urequests.index')}}" class="tiny ui button blue right floated inverted"> <i class="icon arrow left"></i>Back to Requests</a></h1>
        <div class="ui divider"></div> 
        <form class="ui form" method="post" action="{{ route('loans.urequests.store') }}">
            {{ csrf_field() }}
            <div class="field">
                <label>Amount</label>
                <input type="number" id="" name="amount" class="form-control" placeholder="Amount in KShs" required="" autofocus="">
            </div>
            <div class="field">
                <label>Requested Duration(in Months)</label>
                <input type="number" id="" name="duration" class="form-control" placeholder="Period in Months" required="">
            </div>
            <div class="field">
                <label>Kindly provide a Brief Reason for your loan request</label>
                <textarea name="reason" rows="2" required></textarea>
            </div>
            <div class="field">
                <label>Loan Type</label>
                <select name="loan_type" class="ui search dropdown" required>
                    @if($loan_types)
                        @foreach($loan_types as $loan_type)
                            <option value="{{$loan_type->id}}">{{$loan_type->title}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="field">
                <div class="ui checkbox">
                    <input type="checkbox" tabindex="0" class="hidden" required>
                    <label>I agree to the Terms and Conditions</label>
                </div>
            </div>
            <div class="field">
                <input type="submit" value="Submit" class="ui button blue"/>
            </div>
        </form>
    </div>
@endsection