@extends('layouts.app')

@section('content')
<div class="ui">
    <h1>My Guarantees <a href="{{route('loans.index')}}" class="ui tiny button blue right floated inverted"><i class="icon arrow left"></i> Back To Loans</a></h1>
    <div class="ui divider hidden"></div> 
    @if(count($guarantees)>0)
        <div class="ui stackable segment grid">
            <div class="four wide sa-white column">
                <div class="sa-grid sa-grid-tile">
                    <h1 class="sa-grid-tile-number no-margin"> {{$guarantees->total()}}</h1>
                    <h3 class="sa-grid-tile-title no-margin">Guarantees</h3>
                    <small class="sa-grid-tile-description no-margin"></small>
                </div>
            </div>
            @foreach($counts as $key=>$count)
                @if($count!=0)
                    <div class="four wide sa-{{strtolower($key)}} column">
                        <div class="sa-grid sa-grid-tile">
                            <h1 class="sa-grid-tile-number no-margin">{{$count}}</h1>
                            <h3 class="sa-grid-tile-title no-margin">{{$key}}</h3>
                            <small class="sa-grid-tile-description no-margin">
                                <a class="ui button inverted" href="{{route('loans.index')}}">
                                    <i class="eye icon"></i> View All
                                </a>
                            </small>
                        </div>
                    </div>                
                @endif
            @endforeach
        </div>
        <div class="ui hidden divider"></div>
        <div class="ui hidden divider"></div>
        <div class="ui container">
            <table class="ui table">
                <thead>
                    <th>Loan Ref:Num</th>
                    <th>Requested by</th>
                    <th>Amount</th>
                    <th>Requested On</th>
                    <th>Status</th>
                    <th></th>
                </thead>
                @foreach($guarantees as $guarantee)
                    <tr>
                        <td><a href="{{route('loans.show',$guarantee->loan_request_id)}}">{{$guarantee->loan_request_id}}</a></td>
                        <td>{{$guarantee->loan_request->member_profile->fullname}}</td>
                        <td>{{$guarantee->amount}}</td>
                        <td>{{$guarantee->created_at}}</td>
                        <td>{{$guarantee->statusText}}</td>
                        <td>
                            @if($guarantee->status==1)
                                <form class="ui form" method="post" action="{{route('loans.guarantees.approve',$guarantee->id)}}">
                                    {{ csrf_field() }}
                                    <input type="hidden" value="2" name="status">
                                    <input type="submit" value="Activate" class="ui button tiny green"/>
                                    {{ method_field('PUT') }}
                                </form>
                            @else
                                <a class="ui tiny button blue" href="{{route('loans.show',$guarantee->loan_request_id)}}">View Details</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    @else
        <p>There were no loans. So, here is a general Message</p>
    @endif
</div>
@endsection
