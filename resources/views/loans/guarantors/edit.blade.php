@extends('layouts.app')

@section('content')
    <div class="ui segment">
        <h1>Edit Guarantor<a href="{{route('loans.urequests.show', $loan_ref)}}" class="tiny ui button blue right floated inverted"> <i class="icon arrow left"></i>Back To Loan</a></h1>
        <div class="ui divider"></div>
        <form class="ui form" method="post" action="{{route('loans.guarantors.update', [$loan_ref,$guarantor->id])}}">
            {{ csrf_field() }}
            <p>Guarantor Member Number: {{$guarantor->member_profile_id}}</p>
            <p>Guarantor Name: {{$guarantor->member_profile->fullname}}</p>
            <p>Loan Request Ref Number: {{$guarantor->loan_request_id}}</p>
            <div class="field">
                <label>Amount</label>
                <input type="number" id="" name="amount" class="form-control" placeholder="Amount in KShs" value="{{$guarantor->amount}}" required="">
            </div>                        
            <div class="ui hidden divider"></div>
            {{ method_field('PUT') }}
            <input type="submit" value="Submit" class="ui button blue"/>
        </form>
       
    </div>
    <script>     
        $(document).ready(function() {   
            $('select.dropdown').dropdown();
        });
    </script>
@endsection