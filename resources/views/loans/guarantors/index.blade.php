@extends('layouts.app')

@section('content')
<div class="ui container">
    <h1>Loan Guarantors <a href="{{route('loans.guarantors.create',$loan_ref)}}" class="ui tiny button blue right floated"><i class="icon add"></i> Add Guarantor</a></h1>
    <div class="ui divider hidden"></div> 
    @if(count($guarantors)>0)
        <div class="ui stackable segment grid">
            <div class="four wide sa-white column">
                <div class="sa-grid sa-grid-tile">
                    <h1 class="sa-grid-tile-number no-margin"> {{$guarantors->total()}}</h1>
                    <h3 class="sa-grid-tile-title no-margin">Loan Requests</h3>
                    <small class="sa-grid-tile-description no-margin"></small>
                </div>
            </div>
            @if($counts)
                @foreach($counts as $key=>$count)
                    @if($count!=0)
                        <div class="four wide sa-{{strtolower($key)}} column">
                            <div class="sa-grid sa-grid-tile">
                                <h1 class="sa-grid-tile-number no-margin">{{$count}}</h1>
                                <h3 class="sa-grid-tile-title no-margin">{{$key}}</h3>
                                <small class="sa-grid-tile-description no-margin">
                                    <a class="ui button inverted" href="{{route('loans.index')}}">
                                        <i class="eye icon"></i> View All
                                    </a>
                                </small>
                            </div>
                        </div>                
                    @endif
                @endforeach
            @endif
        </div>
        <div class="ui hidden divider"></div>
        <div class="ui hidden divider"></div>
        <div class="ui container">
            <table class="ui celled table">
                <thead>
                    <th>Member</th>
                    <th>Loan Request</th>
                    <th>Amount(in Kshs.)</th>
                    <th>Date</th>
                    <th>Status</th>
                </thead>
                @foreach($guarantors as $guarantor)
                    <tr>
                        <td>{{$guarantor->member_profile()->fullname}}</td>
                        <td>{{$guarantor->loan_request_id}}</td>
                        <td>{{$guarantor->amount}}</td>
                        <td>{{$guarantor->created_at}}</td>
                        <td><span class="sa-{{strtolower($guarantor->statusText)}}">_</span> {{$guarantor->statusText}}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    @else
        <p>There were no guarantors. So, here is a general Message</p>
    @endif
</div>
@endsection