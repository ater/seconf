@extends('layouts.app')

@section('content')
<div class="ui">
    <h1>Loans <a href="{{route('loans.urequests.create')}}" class="ui tiny button blue right floated"><i class="icon clipboard"></i> Request Loan</a></h1>
    <div class="ui divider hidden"></div> 
    @if(count($loans)>0)
        <div class="ui stackable segment grid">
            <div class="four wide sa-white column">
                <div class="sa-grid sa-grid-tile">
                    <h1 class="sa-grid-tile-number no-margin"> {{$loans->total()}}</h1>
                    <h3 class="sa-grid-tile-title no-margin">Loans</h3>
                    <small class="sa-grid-tile-description no-margin"></small>
                </div>
            </div>
            @foreach($counts as $key=>$count)
                @if($count!=0)
                    <div class="four wide sa-{{strtolower($key)}} column">
                        <div class="sa-grid sa-grid-tile">
                            <h1 class="sa-grid-tile-number no-margin">{{$count}}</h1>
                            <h3 class="sa-grid-tile-title no-margin">{{$key}}</h3>
                            <small class="sa-grid-tile-description no-margin">
                                <a class="ui button inverted" href="{{route('loans.index')}}">
                                    <i class="eye icon"></i> View All
                                </a>
                            </small>
                        </div>
                    </div>                
                @endif
            @endforeach
        </div>
        <div class="ui hidden divider"></div>
        <div class="ui hidden divider"></div>
        <div class="ui container">
            <table class="ui table">
                <thead>
                    <th>Reference Number</th>
                    <th>Amount</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th></th>
                </thead>
                @foreach($loans as $loan)
                    <tr>
                        <td><a href="{{route('loans.show',$loan->loan_request_id)}}">{{$loan->loan_request_id}}</a></td>
                        <td>{{$loan->amount}}</td>
                        <td>{{$loan->created_at}}</td>
                        <td>{{$loan->statusText}}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    @else
        <p>There were no loans. So, here is a general Message</p>
    @endif
</div>
@endsection
