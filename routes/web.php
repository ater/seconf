<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/40', 'HomeController@noProfile')->name('noProfile');

Route::prefix('admin')->group(function (){
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');

    // Password reset routes
    Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
    Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
    Route::resource('contributions','Admin\AdminContributionController',['as' => 'admin']);
    Route::get('contributions/cat/{status}','Admin\AdminContributionController@statusCategory')->name('admin.contributions.category.index');
    Route::resource('loans/urequests','Admin\AdminLoanRequestController',['as' => 'admin.loans']);
    Route::resource('loans/repayments','Admin\AdminLoanRepaymentController',['as' => 'admin.loans']);
    Route::resource('loans','Admin\AdminLoanController',['as' => 'admin']);
    Route::put('/loans/updateStatus/{reference}','Admin\AdminLoanController@updateStatus')->name('admin.loans.updateStatus');
    Route::resource('members','Admin\AdminMemberProfileController',['as' => 'admin']);
    Route::resource('users','Admin\AdminPlatformUserController',['as' => 'admin']);

    
    Route::get('/users/createProfile/{email}','Admin\AdminPlatformUserController@createProfile')->name('admin.users.createProfile')->where('email','[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
});
Route::prefix('ccm')->group(function (){
    Route::get('/login', 'Auth\CCMemberLoginController@showLoginForm')->name('ccmember.login');
    Route::post('/login', 'Auth\CCMemberLoginController@login')->name('ccmember.login.submit');

    // Password reset routes
    Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('ccmember.password.email');
    Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('ccmember.password.request');
    Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
    Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('ccmember.password.reset');
    
    Route::resource('loans/urequests/{id}/feedback','CCMember\CCMLoanRequestCCFeedbackController',['as' => 'ccmember.loans.urequests']);
    Route::resource('loans/urequests','CCMember\CCMLoanRequestController',['as' => 'ccmember.loans']);

    Route::get('/', 'CCMemberController@index')->name('ccmember.dashboard');
});
Route::resource('contributions','ContributionController');
Route::get('contributions/cat/{status}','ContributionController@statusCategory')->name('contributions.category.index');
Route::put('loans/urequests/{id}/sendToCommittee','LoanRequestController@sendToCommittee')->name('loans.urequests.sendToCommittee');
Route::resource('loans/urequests','LoanRequestController',['as' => 'loans']);
Route::resource('loans/{loan_ref}/guarantors','GuarantorController',['as' => 'loans']);
Route::resource('loans/{loan_ref}/repayments','LoanRepaymentController',['as' => 'loans']);
Route::get('loans/guarantees','GuarantorController@guarantees')->name('loans.guarantees.index');
Route::put('loans/guarantees/{id}/approve','GuarantorController@approve')->name('loans.guarantees.approve');
Route::resource('loans','LoanController');
Route::get('/profile', 'MemberProfileController@index')->name('profile.index');
Route::get('/profile/edit', 'MemberProfileController@edit')->name('profile.edit');
Route::put('/profile', 'MemberProfileController@update')->name('profile.update');